/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */
public class BankProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
			
            response.setContentType("text/html");
            PrintWriter out=response.getWriter();
            @SuppressWarnings("rawtypes")
		      
            String bank = request.getParameter("bank");
            Map map = new HashMap();
            boolean flag = false;
            try{
                DatabaseConnection dbcon=new DatabaseConnection();
                try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                    String query = "select * from bank where bank_id='"+bank+"'";
                    try (ResultSet rs = dbcon.getResult(query, con)) {
                        while(rs.next()){
                            map.put("bank_id",rs.getString("bank_id"));
                            map.put("bank_name", rs.getString("bank_name"));
                            map.put("bank_email", rs.getString("bank_email"));
                            map.put("bank_city", rs.getString("bank_city"));
                            map.put("bank_address", rs.getString("bank_address"));
                            map.put("bank_location", rs.getString("bank_location"));
                            map.put("bank_contact", rs.getString("bank_contact"));
                            map.put("bank_alternate_contact", rs.getString("bank_alternate_contact"));
                            flag = true;
                        }
                        //System.out.println(map);
                        rs.close();
                    }
                    stmt.close();
                    if(con != null)
                        con.close();
                }
                if(flag) {
                    request.setAttribute("BankData", map);
                    RequestDispatcher rd = request.getRequestDispatcher("bankProfile.jsp");
                    rd.forward(request, response);
                }
                else {
                        RequestDispatcher rd=request.getRequestDispatcher("error.jsp?");
                    	rd.forward(request, response);
                }
            }
            
            catch(SQLException | ServletException | IOException e){
                RequestDispatcher rd=request.getRequestDispatcher("error.jsp?");
		rd.forward(request, response);
            }
        }
}
