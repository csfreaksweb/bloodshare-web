/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */
public class UserProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
			
            response.setContentType("text/html");
            PrintWriter out=response.getWriter();
            @SuppressWarnings("rawtypes")
		      
            String user = request.getParameter("user");
            Map map = new HashMap();
            boolean flag = false;
            try{
                DatabaseConnection dbcon=new DatabaseConnection();
                try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                    String query = "select * from user where user_name='"+user+"'";
                    try (ResultSet rs = dbcon.getResult(query, con)) {
                        while(rs.next()){
                            map.put("user_name",rs.getString("user_name"));
                            map.put("user_email", rs.getString("user_email"));
                            map.put("user_first_name", rs.getString("user_first_name"));
                            map.put("user_last_name", rs.getString("user_last_name"));
                            map.put("user_gender", rs.getString("user_gender"));
                            map.put("user_blood_group", rs.getString("user_blood_group"));
                            map.put("user_city", rs.getString("user_city"));
                            map.put("user_location", rs.getString("user_location"));
                            map.put("user_pic", rs.getString("user_pic"));
                            map.put("user_type_donor", rs.getString("user_type_donor"));
                            map.put("user_available", rs.getString("user_available"));
                            map.put("user_contact", rs.getString("user_contact"));
                            map.put("user_alternate_contact", rs.getString("user_alternate_contact"));
                            map.put("user_thanks_vote", rs.getInt("user_thanks_vote"));
                            map.put("user_report", rs.getInt("user_report"));
                            flag = true;
                        }
                        //System.out.println(map);
                        rs.close();
                    }
                    stmt.close();
                    if(con != null)
                        con.close();
                }
                if(flag) {
                    request.setAttribute("UserData", map);
                    RequestDispatcher rd = request.getRequestDispatcher("userProfile.jsp");
                    rd.forward(request, response);
                }
                else {
                        RequestDispatcher rd=request.getRequestDispatcher("error.jsp?");
                    	rd.forward(request, response);
                }
            }
            
            catch(SQLException | ServletException | IOException e){
                RequestDispatcher rd=request.getRequestDispatcher("error.jsp?");
		rd.forward(request, response);
            }
        }
}
