/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.crud.banks;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */
public class CreateBanks extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            String bank_name = request.getParameter("bank_name");
            String bank_email = request.getParameter("bank_email");            
            String bank_city = request.getParameter("bank_city");            
            String bank_address = request.getParameter("bank_address");            
            String bank_location = request.getParameter("bank_location");
            String bank_contact = request.getParameter("bank_contact");
            String bank_alternate_contact = request.getParameter("bank_alternate_contact");
            String bank_latitude = request.getParameter("bank_latitude");
            String bank_longitude = request.getParameter("bank_longitude");
            String bank_password = request.getParameter("bank_password");
            DatabaseConnection dbcon = new DatabaseConnection();
            try (Connection con = dbcon.setConnection()) {
                String sql = "insert into bank (bank_name, bank_email, bank_city, bank_address, bank_location, bank_contact"
                        + ", bank_alternate_contact, bank_latitude, bank_longitude, bank_password) values (?,?,?,?,?,?,?,?,?,?)";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, bank_name);
                    prep.setString(2, bank_email);
                    prep.setString(3, bank_city);
                    prep.setString(4, bank_address);
                    prep.setString(5, bank_location);
                    prep.setString(6, bank_contact);
                    prep.setString(7, bank_alternate_contact);
                    prep.setString(8, bank_latitude);
                    prep.setString(9, bank_longitude);
                    prep.setString(10, bank_password);
                    prep.executeUpdate();
                    prep.close();
                }
                if(con != null)
                    con.close();
            } 
            response.sendRedirect("../../../adminCrud.jsp?msg=Bank Created Successfully !");	
        }
        catch(NumberFormatException | SQLException | IOException e){
                RequestDispatcher rd=request.getRequestDispatcher("../../../error.jsp?");
		rd.forward(request, response);
        }
    }
}
