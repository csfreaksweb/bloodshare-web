/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.crud.comments;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */
public class CreateComments extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            String comment_user = request.getParameter("comment_user");
            String comment_text = request.getParameter("comment_text");
            
            DatabaseConnection dbcon = new DatabaseConnection();
            try (Connection con = dbcon.setConnection()) {
                String sql = "insert into comment (comment_user, comment_text) values (?,?)";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, comment_user);
                    prep.setString(2, comment_text);
                    prep.executeUpdate();
                    prep.close();
                }
                if(con != null)
                    con.close();
            } 
            response.sendRedirect("../../../adminCrud.jsp?msg=Comment Created Successfully !");	
        }
        catch(NumberFormatException | SQLException | IOException e){
                RequestDispatcher rd=request.getRequestDispatcher("../../../error.jsp?");
		rd.forward(request, response);
        }
    }
}
