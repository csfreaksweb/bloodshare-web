/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.crud.users;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */
public class ReadUsers extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
			
            response.setContentType("text/html");
            PrintWriter out=response.getWriter();
            @SuppressWarnings("rawtypes")
		      
            List list = new ArrayList();
            
            try{
                DatabaseConnection dbcon=new DatabaseConnection();
                try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                    String query = "select * from user";
                    try (ResultSet rs = dbcon.getResult(query, con)) {
                        while(rs.next()){
                            list.add(rs.getString("user_name"));
                            list.add(rs.getString("user_email"));
                            list.add(rs.getString("user_first_name"));
                            list.add(rs.getString("user_last_name"));
                            list.add(rs.getString("user_gender"));
                            list.add(rs.getString("user_blood_group"));
                            list.add(rs.getString("user_city"));
                            list.add(rs.getString("user_location"));
                            list.add(rs.getString("user_pic"));
                            list.add(rs.getString("user_type_donor"));
                            list.add(rs.getString("user_available"));
                            list.add(rs.getString("user_contact"));
                            list.add(rs.getString("user_alternate_contact"));
                            list.add(rs.getInt("user_thanks_vote"));
                            list.add(rs.getInt("user_report"));
                        }
                        rs.close();
                    }
                    stmt.close();
                    if(con != null)
                        con.close();
                }
                
                request.setAttribute("Users", list);
                RequestDispatcher rd = request.getRequestDispatcher("../../../adminjsp/crud/users/readUsers.jsp");
                rd.forward(request, response);
            }
            
            catch(SQLException | ServletException | IOException e){
                RequestDispatcher rd=request.getRequestDispatcher("../../../error.jsp?");
		rd.forward(request, response);
            }
        }
}
