/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.crud.users;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */
public class UpdateUsers extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            String user_name = request.getParameter("user_name");
            String user_email = request.getParameter("user_email");
            String user_first_name = request.getParameter("user_first_name");
            String user_last_name = request.getParameter("user_last_name");
            String user_gender = request.getParameter("user_gender");
            String user_blood_group = request.getParameter("user_blood_group");
            String user_city = request.getParameter("user_city");
            String user_location = request.getParameter("user_location");
            String user_pic = request.getParameter("hidden");
            System.out.println(user_pic);
            String user_type_donor = request.getParameter("user_type_donor");
            String user_available = request.getParameter("user_available");
            String user_contact = request.getParameter("user_contact");
            String user_alternate_contact = request.getParameter("user_alternate_contact");
            int user_thanks_vote = Integer.parseInt(request.getParameter("user_thanks_vote"));
            int user_report = Integer.parseInt(request.getParameter("user_report"));
            
            DatabaseConnection dbcon = new DatabaseConnection();
            try (Connection con = dbcon.setConnection()) {
                String sql = "update user set user_email = ?, user_first_name = ?, user_last_name = ?, user_gender = ?, "
                        + "user_blood_group = ?,  user_city = ?,  user_location = ?,  user_pic = ?,  user_type_donor = ?, "
                        + "user_available = ?,  user_contact = ?,  user_alternate_contact = ?,  user_thanks_vote = ?, "
                        + " user_report = ? where user_name = ? ";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, user_email);
                    prep.setString(2, user_first_name);
                    prep.setString(3, user_last_name);
                    prep.setString(4, user_gender);
                    prep.setString(5, user_blood_group);
                    prep.setString(6, user_city);
                    prep.setString(7, user_location);
                    prep.setString(8, user_pic);
                    prep.setString(9, user_type_donor);
                    prep.setString(10, user_available);
                    prep.setString(11, user_contact);
                    prep.setString(12, user_alternate_contact);
                    prep.setInt(13, user_thanks_vote);
                    prep.setInt(14, user_report);
                    prep.setString(15, user_name);
                    prep.executeUpdate();
                    prep.close();
                }
                if(con != null)
                    con.close();
            } 
            response.sendRedirect("../../../adminCrud.jsp?msg=User Updated Successfully !");	
        }
        catch(NumberFormatException | SQLException | IOException e){
                RequestDispatcher rd=request.getRequestDispatcher("../../../error.jsp?");
		rd.forward(request, response);
        }
    }
}