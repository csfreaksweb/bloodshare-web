/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/CancelUpvote")
public class CancelUpvote extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelUpvote() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		CancelUpvoteReq req = objectmapper.readValue(request.getInputStream(), CancelUpvoteReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String comment_id = req.getComment_id();
                HttpSession session = request.getSession(true);
                String upvote_user = (String)session.getAttribute("session_name");//req.getUpvote_user();
                
                boolean auth = true;
                
                if(upvote_user == null) auth = false;	
                
                DatabaseConnection dbcon = new DatabaseConnection();
                
                boolean flag = false;
                String return_code = "1048";
                
                if(auth) {
                    try (Connection con = dbcon.setConnection()) {
                    String sql = "delete from upvote where user_name = '"+upvote_user+"'  and comment_id = '"+Integer.parseInt(comment_id)+"'";
                    try (PreparedStatement prep = con.prepareStatement(sql)) {
                        prep.executeUpdate();
                        prep.close();
                        flag = true;
                    }
                    if(con != null)
                        con.close();
                    }

                    if(flag) {
                        try (Connection con = dbcon.setConnection ()) {
                            String sql = "UPDATE comment SET comment_upvote =  (comment_upvote - 1), comment_created = comment_created WHERE comment_id='"+comment_id+"'";
                            try (PreparedStatement prep = con.prepareStatement(sql)) {
                            prep.executeUpdate();
                            prep.close();
                            return_code = "0";
                        }
                        if(con != null)
                            con.close();
                        } 
                    }
                }
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
                
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(GiveUpvote.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(GiveUpvote.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class CancelUpvoteReq{
	
    public CancelUpvoteReq() {
        // TODO Auto-generated constructor stub
    }

        private String comment_id;
        private String upvote_user;
        
        
	public String getComment_id() {
		// TODO Auto-generated method stub
		return comment_id;
	}
	public void setComment_id(String comment_id)
	{
	    this.comment_id = comment_id;
	}	
	
        
        public String getUpvote_user() {
		// TODO Auto-generated method stub
		return upvote_user;
        }
	public void setUpvote_user(String upvote_user)
	{
	    this.upvote_user = upvote_user;
	}		
}
