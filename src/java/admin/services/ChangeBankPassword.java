/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/ChangeBankPassword")
public class ChangeBankPassword extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeBankPassword() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @throws java.sql.SQLException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		ChangeBankPasswordReq req = objectmapper.readValue(request.getInputStream(), ChangeBankPasswordReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                HttpSession session = request.getSession(true);
                String bank_id = (String)session.getAttribute("session_name");//req.getUpvote_user();
                boolean auth = true;
                
                if(bank_id == null) auth = false;	
                
                
                String bank_password = req.getBank_password();
               
                //System.out.println(user_contact);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                String return_code = "1048";
                
                try (Connection con = dbcon.setConnection()) {
                String sql = "update bank set bank_password = ? where bank_id = ? ";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, bank_password);
                    prep.setString(2, bank_id);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                catch(Exception e) {
                    return_code = "1048";
                }
                if(con != null)
                    con.close();
                } 
                
                if(flag) {
                    return_code = "0";
                }
                
                if(!auth) return_code = "1048";
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ChangeBankPassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ChangeBankPassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class ChangeBankPasswordReq{
	
    public ChangeBankPasswordReq() {
        // TODO Auto-generated constructor stub
    }

        private String bank_password;
       
        public String getBank_password() {
		// TODO Auto-generated method stub
		return bank_password;
        }
	public void setBank_password(String bank_password)
	{
	    this.bank_password = bank_password;
	}	
       	
}
