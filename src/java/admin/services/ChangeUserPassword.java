/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/ChangeUserPassword")
public class ChangeUserPassword extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangeUserPassword() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		ChangeUserPasswordReq req = objectmapper.readValue(request.getInputStream(), ChangeUserPasswordReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                HttpSession session = request.getSession(true);
                String user_name = (String)session.getAttribute("session_name");//req.getUpvote_user();
                boolean auth = true;
                
                if(user_name == null) auth = false;	
                
                
                String user_password = req.getUser_password();
               
                //System.out.println(user_contact);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                String return_code = "1048";
                
                try (Connection con = dbcon.setConnection()) {
                String sql = "update user set user_password = ? where user_name = ? ";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, user_password);
                    prep.setString(2, user_name);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                catch(Exception e) {
                    return_code = "1048";
                }
                if(con != null)
                    con.close();
                } 
                
                if(flag) {
                    return_code = "0";
                }
                
                if(!auth) return_code = "1048";
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ChangeUserPassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ChangeUserPassword.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class ChangeUserPasswordReq{
	
    public ChangeUserPasswordReq() {
        // TODO Auto-generated constructor stub
    }

        private String user_password;
       
        public String getUser_password() {
		// TODO Auto-generated method stub
		return user_password;
        }
	public void setUser_password(String user_password)
	{
	    this.user_password = user_password;
	}	
       	
}
