package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/Contact")
public class Contact extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Contact() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		ContactReq req = objectmapper.readValue(request.getInputStream(), ContactReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String name = req.getName();
                String email = req.getEmail();
                String message = req.getMessage();
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                try (Connection con = dbcon.setConnection()) {
                String sql = "insert into contact (name, email, message) values (?,?,?)";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, name);
                    prep.setString(2, email);
                    prep.setString(3, message);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                if(con != null)
                    con.close();
                } 
                
                String return_code = "1048";
                String error_string = "Sorry there is some problem.";
                
                if(flag) {
                    return_code = "0";
                    error_string = "Successful";
                }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		ResponseObj.put("error_string", error_string);
		
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(Contact.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(Contact.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class ContactReq{
	
    public ContactReq() {
        // TODO Auto-generated constructor stub
    }

        private String name;
        private String email;
        private String message;
	
        public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
        
        public String getEmail() {
		// TODO Auto-generated method stub
		return email;
        }
        
        public String getMessage() {
		// TODO Auto-generated method stub
		return message;
        }
}
