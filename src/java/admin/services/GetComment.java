/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/GetComment")
public class GetComment extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetComment() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @throws java.sql.SQLException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	/*
		ObjectMapper objectmapper = new ObjectMapper();
		GetCommentReq req = objectmapper.readValue(request.getInputStream(), GetCommentReq.class);
		
                //int token = Integer.parseInt(req.getToken());
                
                //System.out.println(token);
        */        
                response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
                
                DatabaseConnection dbcon = new DatabaseConnection();
                
                HttpSession session = request.getSession(true);
                String current_user = (String)session.getAttribute("session_name");//req.getUpvote_user();
                
                //String return_code = "1048";
                //String error_string = "Sorry, there is some problem.";
                JSONArray comments = new JSONArray();
                
                int index = 0;
                int temp_index = 0;  
                try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                    String query = "select * from comment join user on comment.comment_user = user.user_name";
                    try (ResultSet rs = dbcon.getResult(query, con)) {
                  //      int counter  = 1;
                        while(rs.next()){
                  //          if(counter >= token && counter < token+10) {
                                JSONObject comment = new JSONObject();
                                comment.put("id", rs.getInt("comment_id"));
                                String comment_user =  rs.getString("comment_user");
                                comment_user =  comment_user.substring(0,1).toUpperCase() + comment_user.substring(1).toLowerCase();
                                comment.put("user",comment_user);
                                String comment_text =  rs.getString("comment_text");
                                comment_text =  comment_text.substring(0,1).toUpperCase() + comment_text.substring(1).toLowerCase();
                                comment.put("text",comment_text);
                                String[] str = rs.getString("comment_created").split(" ");
                                
                                Date date = new Date( );
                                SimpleDateFormat curr_date = new SimpleDateFormat ("yyyy-MM-dd");
                                System.out.println(curr_date.format(date));
                                if(!str[0].equals(curr_date.format(date).toString()))comment.put("created", str[0]);
                                else {
                                    str[1] = str[1].substring(0, str[1].length() - 2);
                                    comment.put("created", "Today at "+str[1]);
                                }
                                
                                comment.put("upvote", rs.getString("comment_upvote"));
                                comment.put("pic", rs.getString("user_pic"));
                                comment.put("index", temp_index);
                                comments.put(index, comment);
                                temp_index++;
                                index++;
                  //        }
                  //          counter++;
                        }
                        rs.close();
                  //      return_code = "0";
                  //      error_string = "Successful";
                    }
                    stmt.close();
                    if(con != null)
                        con.close();
                }    
                
                for(int i = 0; i < index; i++) {
                    JSONObject obj = comments.getJSONObject(i);
                    int comment_id = (int) obj.get("id");
                    obj.put("up_flag", "black");
                    
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from upvote where user_name = '"+current_user+"' and comment_id = '"+comment_id+"'";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                obj.put("up_flag", "blue");
                                break;
                            }
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                        con.close();
                    }    
                    comments.put(i, obj);
                }
                
                for(int i = 0; i < index; i++) {
                    JSONObject obj = comments.getJSONObject(i);
                    int comment_id = (int) obj.get("id");
                    obj.put("rep_flag", "#286090");
                    obj.put("rep_flag_show", 0);
                    
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from report where user_name = '"+current_user+"' and comment_id = '"+comment_id+"'";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                obj.put("rep_flag", "#D43F3A");
                                obj.put("rep_flag_show", 1);
                                break;
                            }
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                        con.close();
                    }    
                    comments.put(i, obj);
                }
                
                
                JSONArray comments_rev = new JSONArray();
                if(comments.length() >=1) {
                    int rev = 0;
                    for(int i = comments.length()-1; i >=0 ; i--) {
                        comments_rev.put(rev, comments.get(i));
                        rev++;
                    }
                }
                comments = comments_rev;
                
                printout.print(comments);        
        }
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(GetComment.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(GetComment.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class GetCommentReq{
	
    public GetCommentReq() {
        // TODO Auto-generated constructor stub
    }

        private String token;
        
        public String getToken() {
		// TODO Auto-generated method stub
		return token;
        }
	public void setToken(String token)
	{
	    this.token = token;
	}		
}
