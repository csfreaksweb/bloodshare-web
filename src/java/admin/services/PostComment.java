/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/PostComment")
public class PostComment extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostComment() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @throws java.sql.SQLException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		PostCommentReq req = objectmapper.readValue(request.getInputStream(), PostCommentReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String comment_text = req.getComment_text();
                HttpSession session = request.getSession(true);
                String comment_user = (String)session.getAttribute("session_name");
                boolean auth = true;
                
                if(comment_user == null) auth = false;
                
                //System.out.println(comment_text+" "+comment_user);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                if(auth)
                {try (Connection con = dbcon.setConnection()) {
                String sql = "insert into comment (comment_user, comment_text) values (?,?)";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, comment_user);
                    prep.setString(2, comment_text);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                if(con != null)
                    con.close();
                } 
                }
                String return_code = "1048";
                String error_string = "Sorry there is some problem.";
                String comment_id = "Unsuccessful";
                
                if(flag) {
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from comment";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                comment_id = rs.getString("comment_id");
                            }
                            return_code = "0";
                            error_string = "Successful";
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                            con.close();
                    }
                }
                
                if(!auth) return_code = "1048";
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		ResponseObj.put("error_string", error_string);
		ResponseObj.put("comment_id", comment_id);
		
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(PostComment.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(PostComment.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class PostCommentReq{
	
    public PostCommentReq() {
        // TODO Auto-generated constructor stub
    }

        private String comment_user;
        private String comment_text;
        
        
	public String getComment_user() {
		// TODO Auto-generated method stub
		return comment_user;
	}
	public void setComment_user(String comment_user)
	{
	    this.comment_user = comment_user;
	}	
	
        
        public String getComment_text() {
		// TODO Auto-generated method stub
		return comment_text;
        }
	public void setComment_text(String comment_text)
	{
	    this.comment_text = comment_text;
	}		
}
