package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/RegisterBank")
public class RegisterBank extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterBank() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		RegisterBankReq req = objectmapper.readValue(request.getInputStream(), RegisterBankReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String bank_name = req.getBank_name();
                String bank_email = req.getBank_email();
                String bank_city = req.getBank_city();
                String bank_address = req.getBank_address();
                String bank_location = req.getBank_location();
                String bank_contact = req.getBank_contact();
                String bank_alternate_contact = req.getBank_alternate_contact();
                String bank_latitude = req.getBank_latitude();
                String bank_longitude = req.getBank_longitude();
                String bank_password = req.getBank_password();
                

                //System.out.println(bank_contact);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                try (Connection con = dbcon.setConnection()) {
                String sql = "insert into bank (bank_name, bank_email, bank_city, bank_address, bank_location, bank_contact,bank_alternate_contact,  bank_latitude, bank_longitude, bank_password) values (?,?,?,?,?,?,?,?,?,?)";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, bank_name);
                    prep.setString(2, bank_email);
                    prep.setString(3, bank_city);
                    prep.setString(4, bank_address);
                    prep.setString(5, bank_location);
                    prep.setString(6, bank_contact);
                    prep.setString(7, bank_alternate_contact);
                    prep.setString(8, bank_latitude);
                    prep.setString(9, bank_longitude);
                    prep.setString(10, bank_password);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                if(con != null)
                    con.close();
                } 
                
                String return_code = "1048";
                String error_string = "Sorry there is some problem.";
                String bank_id = "Unsuccessful";
                HttpSession session = request.getSession(true);
                
                if(flag) {
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from bank";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                bank_id = rs.getString("bank_id");
                            }
                            session.setAttribute("session_name", bank_id);
                            session.setAttribute("session_type", "bank");
                            return_code = "0";
                            error_string = "Successful";
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                            con.close();
                    }
                }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		ResponseObj.put("error_string", error_string);
		ResponseObj.put("bank_id", bank_id);
		
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(RegisterBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(RegisterBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class RegisterBankReq{
	
    public RegisterBankReq() {
        // TODO Auto-generated constructor stub
    }

        private String bank_name;
        private String bank_email;
        private String bank_city;
        private String bank_address;
        private String bank_location;
        private String bank_contact;
        private String bank_alternate_contact;
        private String bank_latitude;
	private String bank_longitude;
	private String bank_password;
	
        public String getBank_name() {
		// TODO Auto-generated method stub
		return bank_name;
	}
	public void setBank_name(String bank_name)
	{
	    this.bank_name = bank_name;
	}	
	
        
        public String getBank_email() {
		// TODO Auto-generated method stub
		return bank_email;
        }
	public void setBank_email(String bank_email)
	{
	    this.bank_email = bank_email;
	}	
        
        
	public String getBank_city() {
		// TODO Auto-generated method stub
		return bank_city;
	}
	public void setBank_city(String bank_city)
	{
	    this.bank_city = bank_city;
	}	
        
        
	public String getBank_address() {
		// TODO Auto-generated method stub
		return bank_address;
	}
	public void setBank_address(String bank_address)
	{
	    this.bank_address = bank_address;
	}	
        
        
	public String getBank_location() {
		// TODO Auto-generated method stub
		return bank_location;
	}
	public void setBank_location(String bank_location)
	{
	    this.bank_location = bank_location;
	}	
        
        
	public String getBank_contact() {
		// TODO Auto-generated method stub
		return bank_contact;
	}
	public void setBank_contact(String bank_contact)
	{
	    this.bank_contact = bank_contact;
	}	
        
        
	public String getBank_alternate_contact() {
		// TODO Auto-generated method stub
		return bank_alternate_contact;
	}
	public void setBank_alternate_contact(String bank_alternate_contact)
	{
	    this.bank_alternate_contact = bank_alternate_contact;
	}	 
        
	public String getBank_latitude() {
		// TODO Auto-generated method stub
		return bank_latitude;
	}
	public void setBank_latitude(String bank_latitude)
	{
	    this.bank_latitude = bank_latitude;
	}	
        
        
	public String getBank_longitude() {
		// TODO Auto-generated method stub
		return bank_longitude;
	}
	public void setBank_longitude(String bank_longitude)
	{
	    this.bank_longitude = bank_longitude;
	}	
	
        
        public String getBank_password() {
		// TODO Auto-generated method stub
		return bank_password;
	}
	public void setBank_password(String bank_password)
	{
	    this.bank_password = bank_password;
	}	
}
