/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/RegisterUser")
public class RegisterUser extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterUser() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		RegisterUserReq req = objectmapper.readValue(request.getInputStream(), RegisterUserReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String user_name = req.getUser_name();
                user_name = user_name.toLowerCase();
                String user_email = req.getUser_email();
                String user_first_name = req.getUser_first_name();
                String user_last_name = req.getUser_last_name();
                String user_gender = req.getUser_gender();
                String user_blood_group = req.getUser_blood_group();
                String user_city = req.getUser_city();
                String user_location = req.getUser_location();
                String user_pic = req.getUser_pic();
                String user_type_donor = req.getUser_type_donor();
                String user_available = req.getUser_available();
                String user_contact = req.getUser_contact();
                String user_alternate_contact = req.getUser_alternate_contact();
                String user_password = req.getUser_password();
                HttpSession session = request.getSession(true);
                
                //System.out.println(user_contact);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                String return_code = "1048";
                String error_string = "Sorry there is some problem.";
                String user_id = "Unsuccessful";
                
                try (Connection con = dbcon.setConnection()) {
                String sql = "insert into user (user_name, user_email, user_first_name, user_last_name, user_gender, "
                        + "user_blood_group, user_city, user_location, user_pic, user_type_donor,"
                        + " user_available, user_contact, user_alternate_contact, user_password) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, user_name);
                    prep.setString(2, user_email);
                    prep.setString(3, user_first_name);
                    prep.setString(4, user_last_name);
                    prep.setString(5, user_gender);
                    prep.setString(6, user_blood_group);
                    prep.setString(7, user_city);
                    prep.setString(8, user_location);
                    prep.setString(9, user_pic);
                    prep.setString(10, user_type_donor);
                    prep.setString(11, user_available);
                    prep.setString(12, user_contact);
                    prep.setString(13, user_alternate_contact);
                    prep.setString(14, user_password);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                catch(Exception e) {
                    return_code = "1048";
                    error_string = "User_name already exists.";
                }
                if(con != null)
                    con.close();
                } 
                
                if(flag) {
                    session.setAttribute("session_name", user_name);
                    session.setAttribute("session_type", "user");
                    return_code = "0";
                    error_string = "Successful";        
                }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		ResponseObj.put("error_string", error_string);
                
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(RegisterUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(RegisterUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class RegisterUserReq{
	
    public RegisterUserReq() {
        // TODO Auto-generated constructor stub
    }

        private String user_name;
        private String user_email;
        private String user_first_name;
        private String user_last_name;
        private String user_gender;
        private String user_blood_group;
        private String user_city;
        private String user_location;
        private String user_pic;
        private String user_type_donor;
        private String user_available;
        private String user_contact;
        private String user_alternate_contact;
        private String user_password;

	public String getUser_name() {
		// TODO Auto-generated method stub
		return user_name;
	}
	public void setUser_name(String user_name)
	{
	    this.user_name = user_name;
	}	
	
        
        public String getUser_email() {
		// TODO Auto-generated method stub
		return user_email;
        }
	public void setUser_email(String user_email)
	{
	    this.user_email = user_email;
	}	
        
        
	public String getUser_first_name() {
		// TODO Auto-generated method stub
		return user_first_name;
	}
	public void setUser_first_name(String user_first_name)
	{
	    this.user_first_name = user_first_name;
	}	
	
        
        public String getUser_last_name() {
		// TODO Auto-generated method stub
		return user_last_name;
	}
	public void setUser_last_name(String user_last_name)
	{
	    this.user_last_name = user_last_name;
	}	
	
        
        public String getUser_gender() {
		// TODO Auto-generated method stub
		return user_gender;
	}
	public void setUser_gender(String user_gender)
	{
	    this.user_gender = user_gender;
	}	
        
        
        public String getUser_blood_group() {
		// TODO Auto-generated method stub
		return user_blood_group;
	}
	public void setUser_blood_group(String user_blood_group)
	{
	    this.user_blood_group = user_blood_group;
	}	
        
        
	public String getUser_city() {
		// TODO Auto-generated method stub
		return user_city;
	}
	public void setUser_city(String user_city)
	{
	    this.user_city = user_city;
	}
        
        public String getUser_location() {
		// TODO Auto-generated method stub
		return user_location;
	}
	public void setUser_location(String user_location)
	{
	    this.user_location = user_location;
	}	
        
        
        public String getUser_pic() {
		// TODO Auto-generated method stub
		return user_pic;
	}
	public void setUser_pic(String user_pic)
	{
	    this.user_pic = user_pic;
	}	
        
        
        public String getUser_type_donor() {
		// TODO Auto-generated method stub
		return user_type_donor;
	}
	public void setUser_type_donor(String user_type_donor)
	{
	    this.user_type_donor = user_type_donor;
	}	
        
        
        public String getUser_available() {
		// TODO Auto-generated method stub
		return user_available;
	}
	public void setUser_available(String user_available)
	{
	    this.user_available = user_available;
	}	
        
        
	public String getUser_contact() {
		// TODO Auto-generated method stub
		return user_contact;
	}
	public void setUser_contact(String user_contact)
	{
	    this.user_contact = user_contact;
	}	
        
        
	public String getUser_alternate_contact() {
		// TODO Auto-generated method stub
		return user_alternate_contact;
	}
	public void setUser_alternate_contact(String user_alternate_contact)
	{
	    this.user_alternate_contact = user_alternate_contact;
	}	
        
        public String getUser_password() {
		// TODO Auto-generated method stub
		return user_password;
	}
	public void setUser_password(String user_password)
	{
	    this.user_password = user_password;
	}	
}
