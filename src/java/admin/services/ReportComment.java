/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/ReportComment")
public class ReportComment extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportComment() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		ReportCommentReq req = objectmapper.readValue(request.getInputStream(), ReportCommentReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String comment_id = req.getComment_id();
                HttpSession session = request.getSession(true);
                String report_user = (String)session.getAttribute("session_name");//req.getReport_user();
                boolean auth = true;
                
                if(report_user == null) auth = false;	
                //System.out.println(comment_text+" "+comment_user);
                String return_code = "1048";
                String error_string = "Sorry, you can't report.";

                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = true;
                
                if(auth) {
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from report";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                 if(report_user.equals(rs.getString("user_name")) && comment_id.equals(rs.getString("comment_id"))) {
                                    flag = false;
                                    break;
                                }
                            }
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                        con.close();
                    }    

                    if(flag) {
                        try (Connection con = dbcon.setConnection()) {
                        String sql = "insert into report (user_name, comment_id) values (?,?)";
                        try (PreparedStatement prep = con.prepareStatement(sql)) {
                            prep.setString(1, report_user);
                            prep.setString(2, comment_id);
                            prep.executeUpdate();
                            prep.close();
                        }
                        if(con != null)
                            con.close();
                        }
                    }

                
                    if(flag) {
                        try (Connection con = dbcon.setConnection ()) {
                        String sql = "UPDATE comment SET comment_report =  (comment_report + 1), comment_created = comment_created WHERE comment_id='"+comment_id+"'";
                        try (PreparedStatement prep = con.prepareStatement(sql)) {
                        prep.executeUpdate();
                        prep.close();

                        return_code = "0";
                        error_string = "Comment reported successfully.";
                        }
                        if(con != null)
                            con.close();
                        } 
                    }

                    boolean del_flag = false;

                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from comment where comment_id = '"+comment_id+"'";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                 if(rs.getInt("comment_report") >= 2) {
                                    del_flag = true;
                                    break;
                                }
                            }
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                        con.close();
                    }

                    if(del_flag) {
                        try (Connection con = dbcon.setConnection ()) {
                        String sql = "DELETE from comment WHERE comment_id='"+comment_id+"'";
                        try (PreparedStatement prep = con.prepareStatement(sql)) {
                        prep.executeUpdate();
                        prep.close();
                        }
                        if(con != null)
                            con.close();
                        }
                    }
                }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		ResponseObj.put("error_string", error_string);
                
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ReportComment.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ReportComment.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class ReportCommentReq{
	
    public ReportCommentReq() {
        // TODO Auto-generated constructor stub
    }

        private String comment_id;
        private String report_user;
        
        
	public String getComment_id() {
		// TODO Auto-generated method stub
		return comment_id;
	}
	public void setComment_id(String comment_id)
	{
	    this.comment_id = comment_id;
	}	
	
        
        public String getReport_user() {
		// TODO Auto-generated method stub
		return report_user;
        }
	public void setReport_user(String report_user)
	{
	    this.report_user = report_user;
	}		
}
