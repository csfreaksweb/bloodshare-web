/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/RequestBlood")
public class RequestBlood extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequestBlood() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @throws java.sql.SQLException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		RequestBloodReq req = objectmapper.readValue(request.getInputStream(), RequestBloodReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String req_name = req.getReq_name();
                String req_email = req.getReq_email();
                String req_phone = req.getReq_phone();
                String req_blood_group = req.getReq_blood_group();
                String req_units = req.getReq_units();
                
                
                //System.out.println(req_text+" "+req_user);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                try (Connection con = dbcon.setConnection()) {
                String sql = "insert into requests (req_name, req_email, req_phone, req_blood_group, req_units ) values (?,?,?,?,?)";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, req_name);
                    prep.setString(2, req_email);
                    prep.setString(3, req_phone);
                    prep.setString(4, req_blood_group);
                    prep.setString(5, req_units);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                if(con != null)
                    con.close();
                } 
                
                String return_code = "1048";
                String error_string = "Sorry there is some problem.";
                String req_id = "Unsuccessful";
                
                if(flag) {
                    return_code = "0";
                    error_string = "Successful";
                }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		ResponseObj.put("error_string", error_string);
		ResponseObj.put("req_id", req_id);
		
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(RequestBlood.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(RequestBlood.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class RequestBloodReq{
	
    public RequestBloodReq() {
        // TODO Auto-generated constructor stub
    }

        private String req_name;
        private String req_email;
        private String req_phone;
        private String req_blood_group;
        private String req_units;
        
        
	public String getReq_name() {
		// TODO Auto-generated method stub
		return req_name;
	}
	public String getReq_email() {
		// TODO Auto-generated method stub
		return req_email;
	}
	public String getReq_phone() {
		// TODO Auto-generated method stub
		return req_phone;
	}
	public String getReq_blood_group() {
		// TODO Auto-generated method stub
		return req_blood_group;
	}
	public String getReq_units() {
		// TODO Auto-generated method stub
		return req_units;
	}
}
