/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/SearchBank")
public class SearchBank extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchBank() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @throws java.sql.SQLException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	/*
		ObjectMapper objectmapper = new ObjectMapper();
		SearchBankReq req = objectmapper.readValue(request.getInputStream(), SearchBankReq.class);
		
                int token = Integer.parseInt(req.getToken());
                
                //System.out.println(token);
        */      
          	response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
	      
                DatabaseConnection dbcon = new DatabaseConnection();
                
        //        String return_code = "1048";
        //        String error_string = "Sorry, there is some problem.";
                JSONArray banks = new JSONArray();
                        
                try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                    String query = "select * from bank";
                    try (ResultSet rs = dbcon.getResult(query, con)) {
                        int index = 0;
                        //int counter  = 1;
                        while(rs.next()){
                            //if(counter >= token && counter < token+10) {
                                JSONObject bank = new JSONObject();
                                bank.put("id", rs.getInt("bank_id"));
                                bank.put("name", rs.getString("bank_name"));
                                bank.put("email", rs.getString("bank_email"));
                                bank.put("city", rs.getString("bank_city"));
                                bank.put("address", rs.getString("bank_address"));
                                bank.put("location", rs.getString("bank_location"));
                                bank.put("contact", rs.getString("bank_contact"));
                                bank.put("alternate_contact", rs.getString("bank_alternate_contact"));
                                
                                JSONArray latlng = new JSONArray();
                                latlng.put(0, rs.getString("bank_latitude"));
                                latlng.put(1, rs.getString("bank_longitude"));
                                bank.put("latlng", latlng);
                                banks.put(index, bank);
                                index++;
                            //}
                            //counter++;
                        }
                        rs.close();
                        //return_code = "0";
                        //error_string = "Successful";
                    }
                    stmt.close();
                    if(con != null)
                        con.close();
                }    
                                
                //JSONObject ResponseObj = new JSONObject();
            
                //ResponseObj.put("return_token", token+10 );
		//ResponseObj.put("banks", banks);
                //ResponseObj.put("return_code", return_code );
		//ResponseObj.put("error_string", error_string);
                
                //System.out.println(comments);
                
                printout.print(banks);
                
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(SearchBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(SearchBank.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class SearchBankReq{
	
    public SearchBankReq() {
        // TODO Auto-generated constructor stub
    }

        private String token;
        
        public String getToken() {
		// TODO Auto-generated method stub
		return token;
        }
	public void setToken(String token)
	{
	    this.token = token;
	}		
}
