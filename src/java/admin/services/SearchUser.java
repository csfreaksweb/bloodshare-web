/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/SearchUser")
public class SearchUser extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchUser() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @throws java.sql.SQLException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
	//	ObjectMapper objectmapper = new ObjectMapper();
	//	SearchUserReq req = objectmapper.readValue(request.getInputStream(), SearchUserReq.class);
		
        //      int token = Integer.parseInt(req.getToken());
                
                //System.out.println(token);
        	response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
	        HttpSession session = request.getSession(true);
                String current_user = (String)session.getAttribute("session_name");//req.getUpvote_user();
                
                DatabaseConnection dbcon = new DatabaseConnection();
                
        //        String return_code = "1048";
        //        String error_string = "Sorry, there is some problem.";
                JSONArray users = new JSONArray();
                int index = 0;    
                try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                    String query = "select * from user";
                    try (ResultSet rs = dbcon.getResult(query, con)) {
                    
        //                int counter  = 1;
                        while(rs.next()){
        //                    if(counter >= token && counter < token+10) {
                                JSONObject user = new JSONObject();
                                user.put("name", rs.getString("user_name"));
                                user.put("email", rs.getString("user_email"));
                                user.put("first_name", rs.getString("user_first_name"));
                                user.put("last_name", rs.getString("user_last_name"));
                                user.put("gender", rs.getString("user_gender"));
                                user.put("blood_group", rs.getString("user_blood_group"));
                                user.put("city", rs.getString("user_city"));
                                user.put("location", rs.getString("user_location"));
                                user.put("pic", rs.getString("user_pic"));
                                //user.put("user_type_donor", rs.getString("user_type_donor"));
                                //user.put("user_available", rs.getString("user_available"));
                                user.put("contact", rs.getString("user_contact"));
                                user.put("alternate_contact", rs.getString("user_alternate_contact"));
                                user.put("thanks_vote", rs.getString("user_thanks_vote"));
                                user.put("current_user", current_user);
                                //user.put("user_report", rs.getString("user_report"));
                                users.put(index, user);
                                index++;
        //                    }
        //                    counter++;
                        }
                        rs.close();
        //                return_code = "0";
        //                error_string = "Successful";
                    }
                    
                    
                    stmt.close();
                    if(con != null)
                        con.close();
                }    
                
                for(int i = 0; i < index; i++) {
                    JSONObject obj = users.getJSONObject(i);
                    String user_name = (String)obj.get("name");
                    obj.put("thanks_flag", "black");
                    
                    if(((String)session.getAttribute("session_type")) != null) {
                        if(((String)session.getAttribute("session_type")).equals("user")) obj.put("thanks_flag_show", 1);
                        else obj.put("thanks_flag_show", 0);
                    }
                    else obj.put("thanks_flag_show", 0);
                    
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from thanksvote where user_name = '"+current_user+"' and donor_name = '"+user_name+"'";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                obj.put("thanks_flag", "#c30000");
                                break;
                            }
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                        con.close();
                    }    
                    users.put(i, obj);
                }
                
                printout.print(users);
                
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(SearchUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(SearchUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class SearchUserReq{
	
    public SearchUserReq() {
        // TODO Auto-generated constructor stub
    }

        private String token;
        
        public String getToken() {
		// TODO Auto-generated method stub
		return token;
        }
	public void setToken(String token)
	{
	    this.token = token;
	}		
}
