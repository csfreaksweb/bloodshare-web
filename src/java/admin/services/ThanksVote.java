/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/ThanksVote")
public class ThanksVote extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ThanksVote() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		ThanksVoteReq req = objectmapper.readValue(request.getInputStream(), ThanksVoteReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String donor_name = req.getDonor_name();
                HttpSession session = request.getSession(true);
                String user_name = (String)session.getAttribute("session_name");//req.getUpvote_user();
                boolean auth = true;
                
                if(user_name == null) auth = false;	
                //System.out.println(comment_text+" "+comment_user);
                
                String return_code = "1048";
                String error_string = "You have already thanked.";

                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = true;
                
                if(auth) {
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from thanksvote";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                 if((user_name.equals(rs.getString("user_name")) && donor_name.equals(rs.getString("donor_name"))) || donor_name.equals(user_name)) {
                                    flag = false;
                                    break;
                                }
                            }
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                        con.close();
                    }    

                    if(flag) {
                        try (Connection con = dbcon.setConnection()) {
                        String sql = "insert into thanksvote (donor_name, user_name) values (?,?)";
                        try (PreparedStatement prep = con.prepareStatement(sql)) {
                            prep.setString(1, donor_name);
                            prep.setString(2, user_name);
                            prep.executeUpdate();
                            prep.close();
                        }
                        if(con != null)
                            con.close();
                        }
                    }

                
                    if(flag) {
                        try (Connection con = dbcon.setConnection ()) {
                        String sql = "UPDATE user SET user_thanks_vote =  (user_thanks_vote + 1) WHERE user_name='"+donor_name+"'";
                        try (PreparedStatement prep = con.prepareStatement(sql)) {
                        prep.executeUpdate();
                        prep.close();

                        return_code = "0";
                        error_string = "Thanked successfully.";
                        }
                        if(con != null)
                            con.close();
                        } 
                    }
                }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		ResponseObj.put("error_string", error_string);
                
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ThanksVote.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(ThanksVote.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class ThanksVoteReq{
	
    public ThanksVoteReq() {
        // TODO Auto-generated constructor stub
    }

        private String donor_name;
        
        
	public String getDonor_name() {
		// TODO Auto-generated method stub
		return donor_name;
	}
}
