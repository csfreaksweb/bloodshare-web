/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/UpdateBankProfile")
public class UpdateBankProfile extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateBankProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		UpdateBankProfileReq req = objectmapper.readValue(request.getInputStream(), UpdateBankProfileReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                HttpSession session = request.getSession(true);
                String bank_id = (String)session.getAttribute("session_name");//req.getUpvote_bank();
                boolean auth = true;
                
                if(bank_id == null) auth = false;	
                
                String bank_email = req.getBank_email();
                String bank_city = req.getBank_city();
                String bank_address = req.getBank_address();
                String bank_location = req.getBank_location();
                String bank_contact = req.getBank_contact();
                String bank_alternate_contact = req.getBank_alternate_contact();
                String bank_latitude = req.getBank_latitude();
                String bank_longitude = req.getBank_longitude();
                
                //System.out.println(bank_contact);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                boolean flag = false;
                
                String return_code = "1048";
                
                try (Connection con = dbcon.setConnection()) {
                String sql = "update bank set bank_email = ?, bank_city = ?,   bank_address = ?,  bank_location = ?, bank_contact = ?,  bank_alternate_contact = ? where bank_id = ? ";
                try (PreparedStatement prep = con.prepareStatement(sql)) {
                    prep.setString(1, bank_email);
                    prep.setString(2, bank_city);
                    prep.setString(3, bank_address);
                    prep.setString(4, bank_location);
                    prep.setString(5, bank_contact);
                    prep.setString(6, bank_alternate_contact);
                    prep.setString(7, bank_id);
                    prep.executeUpdate();
                    prep.close();
                    flag = true;
                }
                catch(Exception e) {
                    return_code = "1048";
                }
                if(con != null)
                    con.close();
                } 
                
                if(flag) {
                    return_code = "0";
                }
                
                if(!auth) return_code = "1048";
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		
                printout.print(ResponseObj.toString());        
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(UpdateBankProfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(UpdateBankProfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class UpdateBankProfileReq{
	
    public UpdateBankProfileReq() {
        // TODO Auto-generated constructor stub
    }

        private String bank_email;
        private String bank_city;
        private String bank_address;
        private String bank_location;
        private String bank_contact;
        private String bank_alternate_contact;
        private String bank_latitude;
	private String bank_longitude;
        
        public String getBank_email() {
		// TODO Auto-generated method stub
		return bank_email;
        }
	public void setBank_email(String bank_email)
	{
	    this.bank_email = bank_email;
	}	
        
        
	public String getBank_city() {
		// TODO Auto-generated method stub
		return bank_city;
	}
	public void setBank_city(String bank_city)
	{
	    this.bank_city = bank_city;
	}
        
	public String getBank_address()
	{
	    return bank_address;
	}
	public void setBank_address(String bank_address)
	{
	    this.bank_address = bank_address;
	}
        
        public String getBank_location() {
		// TODO Auto-generated method stub
		return bank_location;
	}
	public void setBank_location(String bank_location)
	{
	    this.bank_location = bank_location;
	}	
        
        
	public String getBank_contact() {
		// TODO Auto-generated method stub
		return bank_contact;
	}
	public void setBank_contact(String bank_contact)
	{
	    this.bank_contact = bank_contact;
	}	
        
        
	public String getBank_alternate_contact() {
		// TODO Auto-generated method stub
		return bank_alternate_contact;
	}
	public void setBank_alternate_contact(String bank_alternate_contact)
	{
	    this.bank_alternate_contact = bank_alternate_contact;
	}	
        
        public String getBank_latitude() {
		// TODO Auto-generated method stub
		return bank_latitude;
	}
	public void setBank_latitude(String bank_latitude)
	{
	    this.bank_latitude = bank_latitude;
	}	
        
        
	public String getBank_longitude() {
		// TODO Auto-generated method stub
		return bank_longitude;
	}
	public void setBank_longitude(String bank_longitude)
	{
	    this.bank_longitude = bank_longitude;
	}	
}
