/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */

@WebServlet("/UpdateProfile")
public class UpdateProfile extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @throws org.json.JSONException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
         
        protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException, SQLException {
		// TODO Auto-generated method stub
	
		ObjectMapper objectmapper = new ObjectMapper();
		UpdateProfileReq req = objectmapper.readValue(request.getInputStream(), UpdateProfileReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                HttpSession session = request.getSession(true);
                String user_name = (String)session.getAttribute("session_name");//req.getUpvote_user();
                boolean auth = true;
                
                if(user_name == null) auth = false;	
                
                String user_email = req.getUser_email();
                String user_pic = req.getUser_pic();
                String user_city = req.getUser_city();
                String user_location = req.getUser_location();
                String user_contact = req.getUser_contact();
                String user_alternate_contact = req.getUser_alternate_contact();
                
                //System.out.println(user_contact);
                
                DatabaseConnection dbcon = new DatabaseConnection();
                
                String return_code = "1048";
                
                if(auth) {
                    try (Connection con = dbcon.setConnection()) {
                        if(user_pic.equals("none")) {
                            String sql = "update user set user_email = ?, user_city = ?,  user_location = ?, user_contact = ?,  user_alternate_contact = ? where user_name = ? ";
                            try (PreparedStatement prep = con.prepareStatement(sql)) {
                                prep.setString(1, user_email);
                                prep.setString(2, user_city);
                                prep.setString(3, user_location);
                                prep.setString(4, user_contact);
                                prep.setString(5, user_alternate_contact);
                                prep.setString(6, user_name);
                                prep.executeUpdate();
                                prep.close();
                                return_code = "0";
                            }
                            catch(Exception e) {
                                return_code = "1048";
                            }
                        }
                        else{
                            String sql = "update user set user_email = ?, user_pic = ?, user_city = ?,  user_location = ?, user_contact = ?,  user_alternate_contact = ? where user_name = ? ";
                            try (PreparedStatement prep = con.prepareStatement(sql)) {
                                prep.setString(1, user_email);
                                prep.setString(2, user_pic);
                                prep.setString(3, user_city);
                                prep.setString(4, user_location);
                                prep.setString(5, user_contact);
                                prep.setString(6, user_alternate_contact);
                                prep.setString(7, user_name);
                                prep.executeUpdate();
                                prep.close();
                                return_code = "0";
                            }
                            catch(Exception e) {
                                return_code = "1048";
                            }
                        }
                        if(con != null)
                            con.close();
                        }
                    }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
		
                printout.print(ResponseObj.toString());
	}
	
        @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(UpdateProfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
	
        @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // TODO Auto-generated method stub
            processRequest(request, response);
        } catch (JSONException | SQLException ex) {
            Logger.getLogger(UpdateProfile.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
		
}

class UpdateProfileReq{
	
    public UpdateProfileReq() {
        // TODO Auto-generated constructor stub
    }

        private String user_email;
        private String user_pic;
        private String user_city;
        private String user_location;
        private String user_contact;
        private String user_alternate_contact;
        
        public String getUser_email() {
		// TODO Auto-generated method stub
		return user_email;
        }
	public void setUser_email(String user_email)
	{
	    this.user_email = user_email;
	}	
        
        
        public String getUser_pic() {
		// TODO Auto-generated method stub
		return user_pic;
        }
	public void setUser_pic(String user_pic)
	{
	    this.user_pic = user_pic;
	}	
        
        
	public String getUser_city() {
		// TODO Auto-generated method stub
		return user_city;
	}
	public void setUser_city(String user_city)
	{
	    this.user_city = user_city;
	}
        
        public String getUser_location() {
		// TODO Auto-generated method stub
		return user_location;
	}
	public void setUser_location(String user_location)
	{
	    this.user_location = user_location;
	}	
        
        
	public String getUser_contact() {
		// TODO Auto-generated method stub
		return user_contact;
	}
	public void setUser_contact(String user_contact)
	{
	    this.user_contact = user_contact;
	}	
        
        
	public String getUser_alternate_contact() {
		// TODO Auto-generated method stub
		return user_alternate_contact;
	}
	public void setUser_alternate_contact(String user_alternate_contact)
	{
	    this.user_alternate_contact = user_alternate_contact;
	}	
}
