/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login.users;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import util.DatabaseConnection;

/**
 *
 * @author shivam_sg
 */
public class checkUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JSONException {
                
                ObjectMapper objectmapper = new ObjectMapper();
		checkUserReq req = objectmapper.readValue(request.getInputStream(), checkUserReq.class);
		response.setContentType("application/json; charset=UTF-8");
                PrintWriter printout = response.getWriter();
		
                String user_name = req.getUser_name();
                String user_password = req.getUser_password();
                HttpSession session = request.getSession(true);
                //System.out.print(user_name);
                String return_code = "1048";
                try{
                    DatabaseConnection dbcon=new DatabaseConnection();
                    try (Connection con = dbcon.setConnection (); Statement stmt = (Statement) con.createStatement()) {
                        String query = "select * from user where user_name ='"+user_name+"' and user_password ='"+user_password+"'";
                        try (ResultSet rs = dbcon.getResult(query, con)) {
                            while(rs.next()){
                                    session.setAttribute("session_name", user_name);
                                    session.setAttribute("session_type", "user");
                                    //System.out.print(session.getAttribute("session_name"));
                                    return_code = "0";
                            }
                            rs.close();
                        }
                        stmt.close();
                        if(con != null)
                            con.close();
                    }
                }
            
                catch(SQLException e){
                    response.sendRedirect("../../error.jsp");
                }
                
                JSONObject ResponseObj = new JSONObject();
            
                ResponseObj.put("return_code", return_code );
                printout.print(ResponseObj.toString());   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(checkUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JSONException ex) {
            Logger.getLogger(checkUser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

class checkUserReq{
	
    public checkUserReq() {
        // TODO Auto-generated constructor stub
    }

        private String user_name;
        private String user_password;

	public String getUser_name() {
		// TODO Auto-generated method stub
		return user_name;
	}
	public void setUser_name(String user_name)
	{
	    this.user_name = user_name;
	}	

        public String getUser_password() {
		// TODO Auto-generated method stub
		return user_password;
	}
	public void setUser_password(String user_password)
	{
	    this.user_password = user_password;
	}	
}
