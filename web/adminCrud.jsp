<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Admin CRUD</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div>
            <h3>:::Admin CRUD:::</h3>
            <ul>
                <li><a href="admin/crud/users/ReadUsers">Users</a></li>
                <li><a href="admin/crud/banks/ReadBanks">Banks</a></li>
                <li><a href="admin/crud/comments/ReadComments">Comments</a></li>
            </ul>
            
            <h4>
                <font style="color: green;">
                    <%
                        if(request.getParameter("msg") != null )
                            out.println(request.getParameter("msg"));
                    %>
                </font>
            </h4>
        </div>
    </body>
</html>
