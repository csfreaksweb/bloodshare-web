<%-- 
    Document   : readbanks
    Created on : 3 Jan, 2016, 1:16:23 AM
    Author     : shivam_sg
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="../../../error.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Banks</title>
    </head>
    <body>
        <h1>Banks</h1>
        <form name="bank_form" action="../../../admin/crud/banks/CreateBanks" method="post">
           <table>
                <tr>
                    <td>bank_name</td>
                    <td><input type="text" placeholder="Enter the bank_name" name="bank_name" id="bank_name"></td>
                </tr>
                <tr>
                    <td>bank_email</td>
                    <td><input type="text" placeholder="Enter the bank_email" name="bank_email" id="bank_email"></td>
                </tr>
                <tr>
                    <td>bank_city</td>
                    <td><input type="text" placeholder="Enter the bank_city" name="bank_city" id="bank_city"></td>
                </tr>
                <tr>
                    <td>bank_address</td>
                    <td><input type="text" placeholder="Enter the bank_address" name="bank_address" id="bank_address"></td>
                </tr>
                <tr>
                    <td>bank_location</td>
                    <td><input type="text" placeholder="Enter the bank_location" name="bank_location" id="bank_location"></td>
                </tr>
                <tr>
                    <td>bank_contact</td>
                    <td><input type="text" placeholder="Enter the bank_contact" name="bank_contact" id="bank_contact"></td>
                </tr>
                <tr>
                    <td>bank_alternate_contact</td>
                    <td><input type="text" placeholder="Enter the bank_alternate_contact" name="bank_alternate_contact" id="bank_alternate_contact"></td>
                </tr>
                <tr>
                    <td>bank_latitude</td>
                    <td><input type="text" placeholder="Enter the bank_latitude" name="bank_latitude" id="bank_latitude"></td>
                </tr>
                <tr>
                    <td>bank_longitude</td>
                    <td><input type="text" placeholder="Enter the bank_longitude" name="bank_longitude" id="bank_longitude"></td>
                </tr>
                <tr>
                    <td>bank_password</td>
                    <td><input type="text" placeholder="Enter the bank_password" name="bank_password" id="bank_password"></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Create"></td>
                </tr>
            </table>
            <input type="hidden" id="hidden" name="hidden">
        </form>
        <br>
        <table style="border: 2px solid; ">
            <tr>
                <td>S.No</td>
                <td>bank_id</td>
                <td>bank_name</td>
                <td>bank_email</td>
                <td>bank_city</td>
                <td>bank_address</td>
                <td>bank_location</td>
                <td>bank_contact</td>
                <td>bank_alternate_contact</td>
                <td>bank_report</td>
                <td>bank_latitude</td>
                <td>bank_longitude</td>
                <td colspan="2">Action</td>
            </tr>
            <%Iterator itr;
            int count = 1; 
            List data=(List) request.getAttribute("Banks");
		for(itr = data.iterator(); itr.hasNext();){
		%>
		
		<tr>
			<% String s= (String)itr.next(); %>
			<td><%=count%></td>
			<td><%=s%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><a href="../../../adminjsp/crud/banks/updateBanks.jsp?val=<%=s%>" style='text-decoration:none; color:blue;'>Edit</a></td>
			<td><a href="../../../adminjsp/crud/banks/deleteBanks.jsp?val=<%=s%>" style='text-decoration:none; color:blue;'>Delete</a></td>
		<%
                    count++;
                }%>
		</tr>
        </table>
    </body>
</html>
