<%-- 
    Document   : updateBanks
    Created on : 6 Jan, 2016, 1:19:03 PM
    Author     : shivam_sg
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../databaseConnection.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Banks</title>
    </head>
    <body>
        
        <%
            int bank_id = Integer.parseInt(request.getParameter("val"));
            
            String sql = "select * from bank where bank_id ='"+bank_id+"'";
            
            Statement stmt = (Statement) con.createStatement();
            ResultSet rs = stmt.executeQuery(sql); 
            
            while(rs.next()){
                %>
                <form name="bank_form" action="../../../admin/crud/banks/UpdateBanks" method="post">
                    <table>
                        <tr>
                            <td>bank_id</td>
                            <td><input type="text" value="<%out.println(rs.getInt("bank_id"));%>" placeholder="Enter the bank_id" name="bank_id" id="bank_id" readonly></td>
                        </tr>
                        <tr>
                            <td>bank_name</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_name"));%>" placeholder="Enter the bank_name" name="bank_name" id="bank_name"></td>
                        </tr>
                        <tr>
                            <td>bank_email</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_email"));%>" placeholder="Enter the bank_email" name="bank_email" id="bank_email"></td>
                        </tr>
                        <tr>
                            <td>bank_city</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_city"));%>" placeholder="Enter the bank_city" name="bank_city" id="bank_city"></td>
                        </tr>
                        <tr>
                            <td>bank_address</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_address"));%>" placeholder="Enter the bank_address" name="bank_address" id="bank_address"></td>
                        </tr>
                        <tr>
                            <td>bank_location</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_location"));%>" placeholder="Enter the bank_location" name="bank_location" id="bank_location"></td>
                        </tr>
                        <tr>
                            <td>bank_contact</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_contact"));%>" placeholder="Enter the bank_contact" name="bank_contact" id="bank_contact"></td>
                        </tr>
                        <tr>
                            <td>bank_alternate_contact</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_alternate_contact"));%>" placeholder="Enter the bank_alternate_contact" name="bank_alternate_contact" id="bank_alternate_contact"></td>
                        </tr>
                        <tr>
                            <td>bank_report</td>
                            <td><input type="text" value="<%out.println(rs.getInt("bank_report"));%>" placeholder="Enter the bank_report" name="bank_report" id="bank_report"></td>
                        </tr>
                        <tr>
                            <td>bank_latitude</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_latitude"));%>" placeholder="Enter the bank_latitude" name="bank_latitude" id="bank_latitude"></td>
                        </tr>
                        <tr>
                            <td>bank_bank_longitude</td>
                            <td><input type="text" value="<%out.println(rs.getString("bank_longitude"));%>" placeholder="Enter the bank_longitude" name="bank_longitude" id="bank_longitude"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Update">
                            </td>
                        </tr>
                    </table>
                </form>
        
                <%
            }
            rs.close();
            if(con != null)
                con.close();
        %>
        
    </body>
</html>
