<%-- 
    Document   : deleteComments
    Created on : 6 Jan, 2016, 12:38:47 PM
    Author     : shivam_sg
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../databaseConnection.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete Comments</title>
    </head>
    <body>
        
        <%
            String comment_id = request.getParameter("val");
            
            String sql = "select * from comment where comment_id ='"+comment_id+"'";
            
            Statement stmt=(Statement) con.createStatement();
            ResultSet rs=stmt.executeQuery(sql); 
            
            while(rs.next()){
                %>
                <form name="comment_form" action="../../../admin/crud/comments/DeleteComments" method="post">
                    <table>
                        <tr>
                            <td>comment_id</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_id"));%>" placeholder="Enter the comment_id" name="comment_id" id="comment_id" readonly></td>
                        </tr>
                        <tr>
                            <td>comment_user</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_user"));%>" placeholder="Enter the comment_user" name="comment_user" id="comment_user" readonly></td>
                        </tr>
                        <tr>
                            <td>comment_text</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_text"));%>" placeholder="Enter the comment_text" name="comment_text" id="comment_text" readonly></td>
                        </tr>
                        <tr>
                            <td>comment_created</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_created"));%>" placeholder="Enter the comment_created" name="comment_created" id="comment_created" readonly></td>
                        </tr>
                        <tr>
                            <td>comment_upvote</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_upvote"));%>" placeholder="Enter the comment_upvote" name="comment_upvote" id="comment_upvote" readonly></td>
                        </tr>
                        <tr>
                            <td>comment_report</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_report"));%>" placeholder="Enter the comment_report" name="comment_report" id="comment_report" readonly></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Delete">
                            </td>
                        </tr>
                    </table>
                </form>
        
                <%
            }
            rs.close();
            if(con != null)
                con.close();
        %>
        
    </body>
</html>
