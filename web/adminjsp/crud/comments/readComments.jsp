<%-- 
    Document   : readcomments
    Created on : 3 Jan, 2016, 1:16:23 AM
    Author     : shivam_sg
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="../../../error.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comments</title>
    </head>
    <body>
        <h1>Comments</h1>
        <form name="comment_form" action="../../../admin/crud/comments/CreateComments" method="post">
           <table>
                <tr>
                    <td>comment_user</td>
                    <td><input type="text" placeholder="Enter the comment_user" name="comment_user" id="comment_user"></td>
                </tr>
                <tr>
                    <td>comment_text</td>
                    <td><input type="text" placeholder="Enter the comment_text" name="comment_text" id="comment_text"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" value="Create">
                    </td>
                </tr>
            </table>
            <input type="hidden" id="hidden" name="hidden">
        </form>
        <br>
        <table style="border: 2px solid; ">
            <tr>
                <td>S.No</td>
                <td>comment_id</td>
                <td>comment_user</td>
                <td>comment_text</td>
                <td>comment_created</td>
                <td>comment_upvote</td>
                <td>comment_report</td>
                <td colspan="2">Action</td>
            </tr>
            <%Iterator itr;
            int count = 1; 
            List data=(List) request.getAttribute("Comments");
		for(itr = data.iterator(); itr.hasNext();){
		%>
		
		<tr>
			<% String s= (String)itr.next(); %>
			<td><%=count%></td>
			<td><%=s%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><a href="../../../adminjsp/crud/comments/updateComments.jsp?val=<%=s%>" style='text-decoration:none; color:blue;'>Edit</a></td>
			<td><a href="../../../adminjsp/crud/comments/deleteComments.jsp?val=<%=s%>" style='text-decoration:none; color:blue;'>Delete</a></td>
		<%
                    count++;
                }%>
		</tr>
        </table>
    </body>
</html>
