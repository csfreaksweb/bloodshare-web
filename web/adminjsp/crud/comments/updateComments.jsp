<%-- 
    Document   : updateComments
    Created on : 6 Jan, 2016, 1:55:38 AM
    Author     : shivam_sg
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../databaseConnection.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Comments</title>
    </head>
    <body>
        
        <%
            int comment_id = Integer.parseInt(request.getParameter("val"));
            
            String sql = "select * from comment where comment_id ='"+comment_id+"'";
            
            Statement stmt = (Statement) con.createStatement();
            ResultSet rs = stmt.executeQuery(sql); 
            
            while(rs.next()){
                %>
                <form name="comment_form" action="../../../admin/crud/comments/UpdateComments" method="post">
                    <table>
                        <tr>
                            <td>comment_id</td>
                            <td><input type="text" value="<%out.println(rs.getInt("comment_id"));%>" placeholder="Enter the comment_id" name="comment_id" id="comment_id"></td>
                        </tr>
                        <tr>
                            <td>comment_user</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_user"));%>" placeholder="Enter the comment_user" name="comment_user" id="comment_user"></td>
                        </tr>
                        <tr>
                            <td>comment_text</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_text"));%>" placeholder="Enter the comment_text" name="comment_text" id="comment_text"></td>
                        </tr>
                        <tr>
                            <td>comment_created</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_created"));%>" placeholder="Enter the comment_created" name="comment_created" id="comment_created" readonly></td>
                        </tr>
                        <tr>
                            <td>comment_upvote</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_upvote"));%>" placeholder="Enter the comment_upvote" name="comment_upvote" id="comment_upvote"></td>
                        </tr>
                        <tr>
                            <td>comment_report</td>
                            <td><input type="text" value="<%out.println(rs.getString("comment_report"));%>" placeholder="Enter the comment_report" name="comment_report" id="comment_report"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Update">
                            </td>
                        </tr>
                    </table>
                </form>
        
                <%
            }
            rs.close();
            if(con != null)
                con.close();
        %>
        
    </body>
</html>
