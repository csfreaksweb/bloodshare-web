<%-- 
    Document   : updateUsers
    Created on : 5 Jan, 2016, 12:45:53 AM
    Author     : shivam_sg
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../databaseConnection.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delete Users</title>
    </head>
    <body>
        
        <%
            String user_name = request.getParameter("val");
            
            String sql = "select * from user where user_name ='"+user_name+"'";
            
            Statement stmt=(Statement) con.createStatement();
            ResultSet rs=stmt.executeQuery(sql); 
            
            while(rs.next()){
                %>
                <form name="user_form" action="../../../admin/crud/users/DeleteUsers" method="post">
                    <table>
                        <tr>
                            <td>user_name</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_name"));%>" placeholder="Enter the user_name" name="user_name" id="user_name" readonly></td>
                        </tr>
                        <tr>
                            <td>user_email</td>
                            <td><input type="email" value="<%out.println(rs.getString("user_email"));%>" placeholder="Enter the user_email" name="user_email" id="user_email" readonly></td>
                        </tr>
                        <tr>
                            <td>user_first_name</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_first_name"));%>" placeholder="Enter the user_first_name" name="user_first_name" id="user_first_name" readonly></td>
                        </tr>
                        <tr>
                            <td>user_last_name</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_last_name"));%>" placeholder="Enter the user_last_name" name="user_last_name" id="user_last_name" readonly></td>
                        </tr>
                        <tr>
                            <td>user_gender</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_gender"));%>" placeholder="Enter the user_gender" name="user_gender" id="user_gender" readonly></td>
                        </tr>
                        <tr>
                            <td>user_blood_group</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_blood_group"));%>" placeholder="Enter the user_blood_group" name="user_blood_group" id="user_blood_group" readonly></td>
                        </tr>
                        <tr>
                            <td>user_city</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_city"));%>" placeholder="Enter the user_city" name="user_city" id="user_city" readonly></td>
                        </tr>
                        <tr>
                            <td>user_location</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_location"));%>" placeholder="Enter the user_location" name="user_location" id="user_location" readonly></td>
                        </tr>
                        <tr>
                            <td>user_pic</td>
                            <td><img src="<%out.println(rs.getString("user_pic"));%>" height="250" width="200" alt="user_pic"></td>
                        </tr>
                        <tr>
                            <td>user_type_donor</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_type_donor"));%>" placeholder="Enter the user_type_donor" name="user_type_donor" id="user_type_donor" readonly></td>
                        </tr>
                        <tr>
                            <td>user_available</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_available"));%>" placeholder="Enter the user_available" name="user_available" id="user_available" readonly></td>
                        </tr>
                        <tr>
                            <td>user_contact</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_contact"));%>" placeholder="Enter the user_contact" name="user_contact" id="user_contact" readonly></td>
                        </tr>
                        <tr>
                            <td>user_alternate_contact</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_alternate_contact"));%>" placeholder="Enter the user_alternate_contact" name="user_alternate_contact" id="user_alternate_contact" readonly></td>
                        </tr>
                        <tr>
                            <td>user_thanks_vote</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_thanks_vote"));%>" placeholder="Enter the user_thanks_vote" name="user_thanks_vote" id="user_thanks_vote" readonly></td>
                        </tr>
                        <tr>
                            <td>user_report</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_report"));%>" placeholder="Enter the user_report" name="user_report" id="user_report" readonly></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Delete">
                            </td>
                        </tr>
                    </table>
                </form>
        
                <%
            }
            rs.close();
            if(con != null)
                con.close();
        %>
        
    </body>
</html>
