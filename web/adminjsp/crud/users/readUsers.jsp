<%-- 
    Document   : readusers
    Created on : 3 Jan, 2016, 1:16:23 AM
    Author     : shivam_sg
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="../../../error.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Users</title>
        <script type="text/javascript" src="../../../js/jquery-1.7.min.js"></script>
        <script type="text/javascript">

        canvasCtx = null;
        imageFile = null;
        url = null;
        flag=0;

        window.onload = function () {
                canvasCtx = document.getElementById("panel").getContext("2d");
                document.getElementById("user_pic").onchange = function(event) {

                        flag=1;		
                        this.imageFile = event.target.files[0];

                        var reader = new FileReader();
                        reader.onload =  function(event) {
                                var img = new Image();
                                img.onload = function() {
                                        drawImage(img);
                                };
                                img.src = event.target.result;
                        };
                        reader.readAsDataURL(this.imageFile);
                };

                drawImage = function(img) {
                        this.canvasCtx.canvas.width = 200;
                        this.canvasCtx.canvas.height = 250;
                        this.canvasCtx.drawImage(img,0,0,200,250);
                        url = canvasCtx.canvas.toDataURL("image/png",0.7);
                        //console.log(url);
                        flag=1;
                        setURL(url,flag);
                        //console.log(document.getElementById("hidden").value);
                };
        };

        function setURL(url,flag)
        {
            document.getElementById("hidden").value= url;	
        }
        
        function validateUser(){
            if(document.getElementById("user_name").value === "")
            {
 		alert("plz fill the user_name");
 		document.user_form.user_name.focus();
 		return false;
            }
            return true;
        }
        
    function loadXMLDoc(){
        var xmlhttp;
        var user_name=document.getElementById("user_name").value;
        var url="../../../adminjsp/crud/users/checkUsers.jsp?user_name="+user_name;

        if (window.XMLHttpRequest){
            xmlhttp=new XMLHttpRequest();
        }
        else{
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
            if (xmlhttp.readyState === 4){
                document.getElementById("err").innerHTML=xmlhttp.responseText;
            }
        } ;
    xmlhttp.open("GET",url,true);
    xmlhttp.send();
    }
     
    </script>
    </head>
    <body>
        <h1>Users</h1>
        <form name="user_form" action="../../../admin/crud/users/CreateUsers" method="post" onsubmit="return validateUser();">
           <table>
                <tr>
                    <td>user_name</td>
                    <td><input type="text" placeholder="Enter the user_name" name="user_name" id="user_name" onkeyup="loadXMLDoc();"></td>
                </tr>
                <tr>
                    <td>user_email</td>
                    <td><input type="email" placeholder="Enter the user_email" name="user_email" id="user_email"></td>
                </tr>
                <tr>
                    <td>user_first_name</td>
                    <td><input type="text" placeholder="Enter the user_first_name" name="user_first_name" id="user_first_name"></td>
                </tr>
                <tr>
                    <td>user_last_name</td>
                    <td><input type="text" placeholder="Enter the user_last_name" name="user_last_name" id="user_last_name"></td>
                </tr>
                <tr>
                    <td>user_gender</td>
                    <td><input type="text" placeholder="Enter the user_gender" name="user_gender" id="user_gender"></td>
                </tr>
                <tr>
                    <td>user_blood_group</td>
                    <td><input type="text" placeholder="Enter the user_blood_group" name="user_blood_group" id="user_blood_group"></td>
                </tr>
                <tr>
                    <td>user_city</td>
                    <td><input type="text" placeholder="Enter the user_city" name="user_city" id="user_city"></td>
                </tr>
                <tr>
                    <td>user_location</td>
                    <td><input type="text" placeholder="Enter the user_location" name="user_location" id="user_location"></td>
                </tr>
                <tr>
                    <td>user_pic</td>
                    <td><input type="file" name="user_pic" id="user_pic"></td>
                </tr>
                <tr>
                    <td></td>
                    <td><canvas id="panel" height="250" width="200"></canvas></td>
                </tr>
                <tr>
                    <td>user_type_donor</td>
                    <td><input type="text" placeholder="Enter the user_type_donor" name="user_type_donor" id="user_type_donor"></td>
                </tr>
                <tr>
                    <td>user_available</td>
                    <td><input type="text" placeholder="Enter the user_available" name="user_available" id="user_available"></td>
                </tr>
                <tr>
                    <td>user_contact</td>
                    <td><input type="text" placeholder="Enter the user_contact" name="user_contact" id="user_contact"></td>
                </tr>
                <tr>
                    <td>user_alternate_contact</td>
                    <td><input type="text" placeholder="Enter the user_alternate_contact" name="user_alternate_contact" id="user_alternate_contact"></td>
                </tr>
                <tr>
                    <td>user_thanks_vote</td>
                    <td><input type="text" placeholder="Enter the user_thanks_vote" name="user_thanks_vote" id="user_thanks_vote"></td>
                </tr>
                <tr>
                    <td>user_report</td>
                    <td><input type="text" placeholder="Enter the user_report" name="user_report" id="user_report"></td>
                </tr>
                <tr>
                    <td>user_password</td>
                    <td><input type="text" placeholder="Enter the user_password" name="user_password" id="user_password"></td>
                </tr>
                <tr>
                    <td>confirm_password</td>
                    <td><input type="text" placeholder="Enter the confirm_password" name="confirm_password" id="confirm_password"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span id="err"></span>
                    </td>
                </tr>
                <input type="hidden" id="hidden" name="hidden">
            </table>
        </form>
        <br>
        <table style="border: 2px solid; ">
            <tr>
                <td>S.No</td>
                <td>user_name</td>
                <td>user_email</td>
                <td>user_first_name</td>
                <td>user_last_name</td>
                <td>user_gender</td>
                <td>user_blood_group</td>
                <td>user_city</td>
                <td>user_location</td>
                <td>user_pic</td>
                <td>user_type_donor</td>
                <td>user_available</td>
                <td>user_contact</td>
                <td>user_alternate_contact</td>
                <td>user_thanks_vote</td>
                <td>user_report</td>
                <td colspan="2">Action</td>
            </tr>
            <%Iterator itr;
            int count = 1; 
            List data=(List) request.getAttribute("Users");
		for(itr = data.iterator(); itr.hasNext();){
		%>
		
		<tr>
			<% String s= (String)itr.next(); %>
			<td><%=count%></td>
			<td><%=s%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
                        <td><img src="<%=itr.next()%>" height="40" width="100" alt="user_pic"></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><%=itr.next()%></td>
			<td><a href="../../../adminjsp/crud/users/updateUsers.jsp?val=<%=s%>" style='text-decoration:none; color:blue;'>Edit</a></td>
			<td><a href="../../../adminjsp/crud/users/deleteUsers.jsp?val=<%=s%>" style='text-decoration:none; color:blue;'>Delete</a></td>
		<%
                    count++;
                }%>
		</tr>
        </table>
    </body>
</html>
