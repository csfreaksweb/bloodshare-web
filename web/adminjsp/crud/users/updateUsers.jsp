<%-- 
    Document   : updateUsers
    Created on : 5 Jan, 2016, 12:45:53 AM
    Author     : shivam_sg
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../../databaseConnection.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Users</title>
        <script type="text/javascript" src="../../../js/jquery-1.7.min.js"></script>
        <script type="text/javascript">

        canvasCtx = null;
        imageFile = null;
        url = null;
        flag=0;

        window.onload = function () {
                canvasCtx = document.getElementById("panel").getContext("2d");
                document.getElementById("user_pic").onchange = function(event) {

                        flag=1;		
                        this.imageFile = event.target.files[0];

                        var reader = new FileReader();
                        reader.onload =  function(event) {
                                var img = new Image();
                                img.onload = function() {
                                        drawImage(img);
                                };
                                img.src = event.target.result;
                        };
                        reader.readAsDataURL(this.imageFile);
                };

                drawImage = function(img) {
                        this.canvasCtx.canvas.width = 200;
                        this.canvasCtx.canvas.height = 250;
                        this.canvasCtx.drawImage(img,0,0,200,250);
                        url = canvasCtx.canvas.toDataURL("image/png",0.7);
                        //console.log(url);
                        flag=1;
                        setURL(url,flag);
                        //console.log(document.getElementById("hidden").value);
                };
        };

        function setURL(url,flag)
        {
            document.getElementById("hidden").value= url;	
            console.log(url);
        }

        </script>
    </head>
    <body>
        
        <%
            String user_name = request.getParameter("val");
            
            String sql = "select * from user where user_name ='"+user_name+"'";
            
            Statement stmt = (Statement) con.createStatement();
            ResultSet rs = stmt.executeQuery(sql); 
            
            while(rs.next()){
                %>
                <form name="user_form" action="../../../admin/crud/users/UpdateUsers" method="post">
                    <table>
                        <tr>
                            <td>user_name</td>
                            <td><input type="text" value="<%out.print(rs.getString("user_name"));%>" placeholder="Enter the user_name" name="user_name" id="user_name" readonly></td>
                        </tr>
                        <tr>
                            <td>user_email</td>
                            <td><input type="email" value="<%out.print(rs.getString("user_email"));%>" placeholder="Enter the user_email" name="user_email" id="user_email"></td>
                        </tr>
                        <tr>
                            <td>user_first_name</td>
                            <td><input type="text" value="<%out.print(rs.getString("user_first_name"));%>" placeholder="Enter the user_first_name" name="user_first_name" id="user_first_name"></td>
                        </tr>
                        <tr>
                            <td>user_last_name</td>
                            <td><input type="text" value="<%out.print(rs.getString("user_last_name"));%>" placeholder="Enter the user_last_name" name="user_last_name" id="user_last_name"></td>
                        </tr>
                        <tr>
                            <td>user_gender</td>
                            <td><input type="text" value="<%out.print(rs.getString("user_gender"));%>" placeholder="Enter the user_gender" name="user_gender" id="user_gender"></td>
                        </tr>
                        <tr>
                            <td>user_blood_group</td>
                            <td><input type="text" value="<%out.print(rs.getString("user_blood_group"));%>" placeholder="Enter the user_blood_group" name="user_blood_group" id="user_blood_group"></td>
                        </tr>
                        <tr>
                            <td>user_city</td>
                            <td><input type="text" value="<%out.print(rs.getString("user_city"));%>" placeholder="Enter the user_city" name="user_city" id="user_city"></td>
                        </tr>
                        <tr>
                            <td>user_location</td>
                            <td><input type="text" value="<%out.print(rs.getString("user_location"));%>" placeholder="Enter the user_location" name="user_location" id="user_location"></td>
                        </tr>
                        <tr>
                            <td>user_pic</td>
                            <td><img src="<%out.println(rs.getString("user_pic"));%>" height="250" width="200" alt="user_pic">
                            </td>
                        </tr>
                        <tr>
                            <td><input type="file" name="user_pic" id="user_pic"></td>
                            <td><canvas id="panel" height="250" width="200"></canvas></td>
                        </tr>
                        <tr>
                            <td>user_type_donor</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_type_donor"));%>" placeholder="Enter the user_type_donor" name="user_type_donor" id="user_type_donor"></td>
                        </tr>
                        <tr>
                            <td>user_available</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_available"));%>" placeholder="Enter the user_available" name="user_available" id="user_available"></td>
                        </tr>
                        <tr>
                            <td>user_contact</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_contact"));%>" placeholder="Enter the user_contact" name="user_contact" id="user_contact"></td>
                        </tr>
                        <tr>
                            <td>user_alternate_contact</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_alternate_contact"));%>" placeholder="Enter the user_alternate_contact" name="user_alternate_contact" id="user_alternate_contact"></td>
                        </tr>
                        <tr>
                            <td>user_thanks_vote</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_thanks_vote"));%>" placeholder="Enter the user_thanks_vote" name="user_thanks_vote" id="user_thanks_vote"></td>
                        </tr>
                        <tr>
                            <td>user_report</td>
                            <td><input type="text" value="<%out.println(rs.getString("user_report"));%>" placeholder="Enter the user_report" name="user_report" id="user_report"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" value="Update">
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" id="hidden" name="hidden">
                </form>
        
                <%
            }
            rs.close();
            if(con != null)
                con.close();
        %>
        
    </body>
</html>
