// Define a new module for our app
var app = angular.module("bankSearch", []);

// Create the instant search filter

app.filter('searchName', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, bank_name){

		if(!bank_name){
			return arr;
		}

		var result = [];

		bank_name = bank_name.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(bank){

			if(bank.name.toLowerCase().indexOf(bank_name) !== -1){
				result.push(bank);
			}

		});

		return result;
	};

});

app.filter('searchCity', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, bank_city){

		if(!bank_city){
			return arr;
		}

		var result = [];

		bank_city = bank_city.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(bank){

			if(bank.city.toLowerCase().indexOf(bank_city) !== -1){
				result.push(bank);
			}

		});

		return result;
	};

});

app.filter('searchLocation', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, bank_location){

		if(!bank_location){
			return arr;
		}

		var result = [];

		bank_location = bank_location.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(bank){

			if(bank.location.toLowerCase().indexOf(bank_location) !== -1){
				result.push(bank);
			}

		});

		return result;
	};

});


// The controller

function BankSearchController($scope, $http){

	// The data model. These banks would normally be requested via AJAX,
	// but are hardcoded here for simplicity. See the next example for
	// tips on using AJAX.

        $http.get("../../admin/services/SearchBank")
            .then(function(response) {
                    $scope.banks = response.data;
                });

}
