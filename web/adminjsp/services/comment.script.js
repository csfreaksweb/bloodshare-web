// Define a new module for our app
var app = angular.module("getComment", []);

// Create the instant search filter

// The controller

function GetCommentController($scope, $http){

	// The data model. These users would normally be requested via AJAX,
	// but are hardcoded here for simplicity. See the next example for
	// tips on using AJAX.

	$http.get("../../admin/services/GetComment")
            .then(function(response) {
                    $scope.comments = response.data;
                });

}
