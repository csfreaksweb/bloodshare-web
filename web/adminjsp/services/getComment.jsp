<%-- 
    Document   : getComment
    Created on : 12 Feb, 2016, 12:04:15 AM
    Author     : shivam_sg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>getComment</title>
        <script type="text/javascript">
        /*
        function getComment() {
                
                var token = document.getElementById("token").value;
                if(token == "") token = 1;
                xmlhttp=new XMLHttpRequest();
                var url = "../../admin/services/GetComment";

                var data = new Object();

                data["token"] = token;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            
                            document.getElementById("return_token").value = json.return_token;
                            document.getElementById("comments").value = JSON.stringify(json.comments);
                            document.getElementById("return_code").value = json.return_code;
                            document.getElementById("error_string").value = json.error_string;
                            //console.log(json.comments);
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }*/
        </script>
    </head>
    <body ng-app="getComment" ng-controller="GetCommentController">
        <h1>Get Comment</h1>
        <table>
            <tr>
                <td>Token</td>
                <td><input type="text" id="token" name="token"></td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
                <td>Return Token</td>
                <td><input type="text" id="return_token" name="return_token"></td>
            </tr>
            <tr>
                <td>Comments</td>
                <td><input type="text" id="comments" name="comments"></td>
            </tr>
            <tr>
                <td>Return Code</td>
                <td><input type="text" id="return_code" name="return_code"></td>
            </tr>
            <tr>
                <td>Error String</td>
                <td><input type="text" id="error_string" name="error_string"></td>
            </tr>
        </table>
        <ul>
            <li ng-repeat="comment in comments">
		<p>{{comment.user}} ----  {{comment.text}} --- {{comment.created}}</p>
            </li>
	</ul>

	<script src="../../js/angular.min.js"></script>
	<script src="comment.script.js"></script>
    </body>
</html>
