<%-- 
    Document   : postComment
    Created on : 12 Feb, 2016, 12:04:27 AM
    Author     : shivam_sg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>postComment</title>
        <script type="text/javascript">
        
        function postComment() {
                
                //var comment_user = document.getElementById("comment_user").value;
                var comment_text = document.getElementById("comment_text").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "../../admin/services/PostComment";

                var data = new Object();

                //data["comment_user"] = comment_user;
                data["comment_text"] = comment_text;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            
                            //document.getElementById("comment_id").value = json.comment_id;
                            document.getElementById("return_code").value = json.return_code;
                            //document.getElementById("error_string").value = json.error_string;		
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }
        </script>
    </head>
    <body>
        <h1>Post Comment</h1>
        <table>
            <tr>
                <td>Comment Text</td>
                <td><input type="text" id="comment_text" name="comment_text"></td>
            </tr>
            <tr>
                <td><input type="button" onclick="postComment()" value="postComment"></td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
                <td>Comment Id</td>
                <td><input type="text" id="comment_id" name="comment_id"></td>
            </tr>
            <tr>
                <td>Return Code</td>
                <td><input type="text" id="return_code" name="return_code"></td>
            </tr>
            <tr>
                <td>Error String</td>
                <td><input type="text" id="error_string" name="error_string"></td>
            </tr>
        </table>
    </body>
</html>
