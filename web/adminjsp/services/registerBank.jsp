<%-- 
    Document   : registerBank
    Created on : 12 Feb, 2016, 12:03:59 AM
    Author     : shivam_sg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>registerBank</title>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?"></script>
        <script type="text/javascript">
        
        function getLocation() {
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById("bank_location").value;
            address = address.concat(", India");
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    //alert("Latitude: " + latitude + "\nLongitude: " + longitude);
                    registerBank(latitude, longitude);
                } else {
                    alert("Invalid Location.");
                }
            });
        }
        
        function registerBank(latitude, longitude) {
                var bank_name = document.getElementById("bank_name").value;
                var bank_email = document.getElementById("bank_email").value;
                var bank_city = document.getElementById("bank_city").value;
                var bank_address = document.getElementById("bank_address").value;
                var bank_location = document.getElementById("bank_location").value;
                var bank_contact = document.getElementById("bank_contact").value;
                var bank_alternate_contact = document.getElementById("bank_alternate_contact").value;
                var bank_password = document.getElementById("bank_password").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "../../admin/services/RegisterBank";

                var data = new Object();

                data["bank_name"] = bank_name;
                data["bank_email"] = bank_email;
                data["bank_city"] = bank_city;
                data["bank_address"] = bank_address;
                data["bank_location"] = bank_location;
                data["bank_contact"] = bank_contact;
                data["bank_alternate_contact"] = bank_alternate_contact;
                data["bank_password"] = bank_password;
                data["bank_latitude"] = latitude;
                data["bank_longitude"] = longitude;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            
                            document.getElementById("bank_id").value = json.bank_id;
                            document.getElementById("return_code").value = json.return_code;
                            document.getElementById("error_string").value = json.error_string;		
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }
        </script>
    </head>
    <body>
        <h1>Register Bank</h1>
        <table>
            <tr>
                <td>Bank Name</td>
                <td><input type="text" id="bank_name" name="bank_name"></td>
            </tr>
            <tr>
                <td>Bank Email</td>
                <td><input type="text" id="bank_email" name="bank_email"></td>
            </tr>
            <tr>
                <td>Bank City</td>
                <td><input type="text" id="bank_city" name="bank_city"></td>
            </tr>
            <tr>
                <td>Bank Address</td>
                <td><input type="text" id="bank_address" name="bank_address"></td>
            </tr>
            <tr>
                <td>Bank Location</td>
                <td><input type="text" id="bank_location" name="bank_location"></td>
            </tr>
            <tr>
                <td>Bank Contact</td>
                <td><input type="text" id="bank_contact" name="bank_contact"></td>
            </tr>
            <tr>
                <td>Bank Alternate Contact</td>
                <td><input type="text" id="bank_alternate_contact" name="bank_alternate_contact"></td>
            </tr>
            <tr>
                <td>Bank Password</td>
                <td><input type="text" id="bank_password" name="bank_password"></td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td><input type="text" id="confirm_password" name="confirm_password"></td>
            </tr>
            <tr>
                <td><input type="button" onclick="getLocation()" value="registerBank"></td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
                <td>Bank Id</td>
                <td><input type="text" id="bank_id" name="bank_id"></td>
            </tr>
            <tr>
                <td>Return Code</td>
                <td><input type="text" id="return_code" name="return_code"></td>
            </tr>
            <tr>
                <td>Error String</td>
                <td><input type="text" id="error_string" name="error_string"></td>
            </tr>
        </table>
    </body>
</html>