<%-- 
    Document   : registerUser
    Created on : 12 Feb, 2016, 12:03:26 AM
    Author     : shivam_sg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>registerUser</title>
        <script type="text/javascript">
        
        canvasCtx = null;
        imageFile = null;
        url = null;
        flag=0;

        window.onload = function () {
                canvasCtx = document.getElementById("panel").getContext("2d");
                document.getElementById("user_pic").onchange = function(event) {

                        flag=1;		
                        this.imageFile = event.target.files[0];

                        var reader = new FileReader();
                        reader.onload =  function(event) {
                                var img = new Image();
                                img.onload = function() {
                                        drawImage(img);
                                };
                                img.src = event.target.result;
                        };
                        reader.readAsDataURL(this.imageFile);
                };

                drawImage = function(img) {
                        this.canvasCtx.canvas.width = 200;
                        this.canvasCtx.canvas.height = 250;
                        this.canvasCtx.drawImage(img,0,0,200,250);
                        url = canvasCtx.canvas.toDataURL("image/png",0.7);
                        //console.log(url);
                        flag=1;
                        setURL(url,flag);
                        //console.log(document.getElementById("hidden").value);
                };
        };

        function setURL(url,flag)
        {
            document.getElementById("hidden").value= url;	
        }

        function registerUser() {
                
                var user_name = document.getElementById("user_name").value;
                var user_email = document.getElementById("user_email").value;
                var user_first_name = document.getElementById("user_first_name").value;
                var user_last_name = document.getElementById("user_last_name").value;
                var user_gender = document.getElementById("user_gender").value;
                var user_blood_group = document.getElementById("user_blood_group").value;
                var user_city = document.getElementById("user_city").value;
                var user_location = document.getElementById("user_location").value;
                var user_pic = document.getElementById("hidden").value;
                var user_type_donor = document.getElementById("user_type_donor").value;
                var user_available = document.getElementById("user_available").value;
                var user_contact = document.getElementById("user_contact").value;
                var user_alternate_contact = document.getElementById("user_alternate_contact").value;
                var user_password = document.getElementById("user_password").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "../../admin/services/RegisterUser";

                var data = new Object();

                data["user_name"] = user_name;
                data["user_email"] = user_email;
                data["user_first_name"] = user_first_name;
                data["user_last_name"] = user_last_name;
                data["user_gender"] = user_gender;
                data["user_blood_group"] = user_blood_group;
                data["user_city"] = user_city;
                data["user_location"] = user_location;
                data["user_pic"] = user_pic;
                data["user_type_donor"] = user_type_donor;
                data["user_available"] = user_available;
                data["user_contact"] = user_contact;
                data["user_alternate_contact"] = user_alternate_contact;
                data["user_password"] = user_password;

                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            
                            document.getElementById("return_code").value = json.return_code;
                            document.getElementById("error_string").value = json.error_string;		
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }
        </script>
    </head>
    <body>
        <h1>Register User</h1>
        <table>
            <tr>
                <td>User Name</td>
                <td><input type="text" id="user_name" name="user_name"></td>
            </tr>
            <tr>
                <td>User Email</td>
                <td><input type="text" id="user_email" name="user_email"></td>
            </tr>
            <tr>
                <td>User First Name</td>
                <td><input type="text" id="user_first_name" name="user_first_name"></td>
            </tr>
            <tr>
                <td>User Last Name</td>
                <td><input type="text" id="user_last_name" name="user_last_name"></td>
            </tr>
            <tr>
                <td>User Gender</td>
                <td><input type="text" id="user_gender" name="user_gender"></td>
            </tr>
            <tr>
                <td>User Blood Group</td>
                <td><input type="text" id="user_blood_group" name="user_blood_group"></td>
            </tr>
            <tr>
                <td>User City</td>
                <td><input type="text" id="user_city" name="user_city"></td>
            </tr>
            <tr>
                <td>User Location</td>
                <td><input type="text" id="user_location" name="user_location"></td>
            </tr>
            <tr>
                <td>user_pic</td>
                <td><input type="file" name="user_pic" id="user_pic"></td>
            </tr>
            <tr>
                <td></td>
                <td><canvas id="panel" height="250" width="200"></canvas></td>
            </tr>
            <tr>
                <td>User Type Donor</td>
                <td><input type="text" id="user_type_donor" name="user_type_donor"></td>
            </tr>
            <tr>
                <td>User Available</td>
                <td><input type="text" id="user_available" name="user_available"></td>
            </tr>
            <tr>
                <td>User Contact</td>
                <td><input type="text" id="user_contact" name="user_contact"></td>
            </tr>
            <tr>
                <td>User Alternate Contact</td>
                <td><input type="text" id="user_alternate_contact" name="user_alternate_contact"></td>
            </tr>
            <tr>
                <td>User Password</td>
                <td><input type="text" id="user_password" name="user_password"></td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td><input type="text" id="confirm_password" name="confirm_password"></td>
            </tr>
            <tr>
                <td><input type="button" onclick="registerUser()" value="registerUser"></td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
                <td>Return Code</td>
                <td><input type="text" id="return_code" name="return_code"></td>
            </tr>
            <tr>
                <td>Error String</td>
                <td><input type="text" id="error_string" name="error_string"></td>
            </tr>
            <input type="hidden" id="hidden" name="hidden">
        </table>
    </body>
</html>
