<%-- 
    Document   : reportComment
    Created on : 12 Feb, 2016, 4:25:08 PM
    Author     : shivam_sg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>reportComment</title>
        <script type="text/javascript">
        
        function reportComment() {
                
                var comment_id = document.getElementById("comment_id").value;
                //var report_user = document.getElementById("report_user").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "../../admin/services/ReportComment";

                var data = new Object();

                data["comment_id"] = comment_id;
                //data["report_user"] = report_user;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            
                            document.getElementById("return_code").value = json.return_code;
                            document.getElementById("error_string").value = json.error_string;		
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }
        </script>
    </head>
    <body>
        <h1>Report Comment</h1>
        <table>
            <tr>
                <td>Comment Id</td>
                <td><input type="text" id="comment_id" name="comment_id"></td>
            </tr>
            <tr>
                <td>Report User</td>
                <td><input type="text" id="report_user" name="report_user"></td>
            </tr>
            <tr>
                <td><input type="button" onclick="reportComment()" value="report"></td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
                <td>Return Code</td>
                <td><input type="text" id="return_code" name="return_code"></td>
            </tr>
            <tr>
                <td>Error String</td>
                <td><input type="text" id="error_string" name="error_string"></td>
            </tr>
        </table>
    </body>
</html>
