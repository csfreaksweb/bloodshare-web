<%-- 
    Document   : getComment
    Created on : 12 Feb, 2016, 12:04:15 AM
    Author     : shivam_sg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>searchBank</title>
        <script type="text/javascript">
        /*
        function searchBank() {
                
                //var token = document.getElementById("token").value;
                //if(token === "") token = 1;
                xmlhttp=new XMLHttpRequest();
                var url = "../../admin/services/SearchBank";

                //var data = new Object();

                //data["token"] = token;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var banks = xmlhttp.responseText;
                            
                            //document.getElementById("return_token").value = json.return_token;
                            //document.getElementById("banks").value = JSON.stringify(json.banks);
                            //document.getElementById("return_code").value = json.return_code;
                            //document.getElementById("error_string").value = json.error_string;
                            console.log(banks);
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                //xmlhttp.send(JSON.stringify(data));
                xmlhttp.send();
        }*/
        </script>
    </head>
    <body ng-app="bankSearch" ng-controller="BankSearchController">
        <h1>Search Bank</h1>
        <table>
            <tr>
                <td>Name</td>
                <td><input type="text" ng-model="bank_name" id="bank_name" name="bank_name"></td>
            </tr>
            <tr>
                <td>City</td>
                <td><input type="text" ng-model="bank_city" id="bank_city" name="bank_city"></td>
            </tr>
            <tr>
                <td>Location</td>
                <td><input type="text" ng-model="bank_location" id="bank_location" name="bank_location"></td>
            </tr>
            <tr>
                <td>Token</td>
                <td><input type="text" id="token" name="token"></td>
            </tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr>
                <td>Return Token</td>
                <td><input type="text" id="return_token" name="return_token"></td>
            </tr>
            <!--
            <tr>
                <td>Banks</td>
                <td><input type="text" id="banks" name="banks"></td>
            </tr>
            -->
            <tr>
                <td>Return Code</td>
                <td><input type="text" id="return_code" name="return_code"></td>
            </tr>
            <tr>
                <td>Error String</td>
                <td><input type="text" id="error_string" name="error_string"></td>
            </tr>
        </table>
        
	<ul>
            <li ng-repeat="bank in banks | searchName:bank_name | searchCity:bank_city | searchLocation:bank_location">
		<p>{{bank.name}} ----  {{bank.city}} --- {{bank.location}}</p>
            </li>
	</ul>

	<script src="../../js/angular.min.js"></script>
	<script src="bank.script.js"></script>
    </body>
</html>
