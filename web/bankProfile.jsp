<%-- 
    Document   : editUser
    Created on : May 9, 2016, 7:25:13 PM
    Author     : shivam_sg
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
        <script src="js/angular.min.js"></script>
        <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-toggle.min.js"></script>
        <script src = "js/loginform.js"></script>
        
        <style>
          body {
              font: 400 15px Lato, sans-serif;
              line-height: 1.8;
              color: #818181;
          }
          
          #map {
              height: 100%;
          }
            
          .controls {
              margin-top: 10px;
              border: 1px solid transparent;
              border-radius: 2px 0 0 2px;
              box-sizing: border-box;
              -moz-box-sizing: border-box;
              height: 32px;
              outline: none;
              box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            }

            #pac-input {
              background-color: #fff;
              font-family: Roboto;
              font-size: 15px;
              font-weight: 300;
              margin-right: 12px;
              padding: 0 11px 0 13px;
              text-overflow: ellipsis;
              width: 300px;
            }

            #pac-input:focus {
              border-color: #4d90fe;
            }

            .pac-container {
              font-family: Roboto;
            }

            #type-selector {
              color: #fff;
              background-color: #4d90fe;
              padding: 5px 11px 0px 11px;
            }

            #type-selector label {
              font-family: Roboto;
              font-size: 13px;
              font-weight: 300;
            }
            #target {
              width: 345px;
            }
            /*.map-icon-label .map-icon {
                    font-size: 12px;
                    color: #000;
                    line-height: 24px;
                    text-align: center;
                    white-space: nowrap;
            }*/
            .infobox-wrapper {
                 border-radius: 15px;
            }
            #infobox {
                border:2px solid black;
                margin-top: 8px;
                background:#333;
                color:#fff;
                font-family: 'Roboto Condensed', sans-serif;
                font-size:15px;
                padding: .5em 1em;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                text-shadow:0 -1px #000000;
                -webkit-box-shadow: 10px 10px 5px #888888;
                box-shadow: 10px 10px 5px #888888;
            }
            .mylabel {
                color:red;
            }
          
            h2 {
              font-size: 24px;
              text-transform: uppercase;
              color: #303030;
              font-weight: 600;
              margin-bottom: 30px;
          }
          h4 {
              font-size: 19px;
              line-height: 1.375em;
              color: #303030;
              font-weight: 400;
              margin-bottom: 30px;
          }  
          .jumbotron {
              background-color: #ff4d4d;
              color: #fff;
              padding: 100px 25px;
              font-family: Montserrat, sans-serif;
              max-height: 300px;
          }
          .container-fluid {
              padding: 60px 50px;
          }
          .bg-grey {
              background-color: #f6f6f6;
          }
          .logo-small {
              color: #c30000;
              font-size: 50px;
          }
          .logo {
              color: #c30000;
              font-size: 200px;
          }
          .thumbnail {
              padding: 0 0 15px 0;
              border: none;
              border-radius: 0;
          }
          .thumbnail img {
              width: 100%;
              height: 100%;
              margin-bottom: 10px;
          }
          .carousel-control.right, .carousel-control.left {
              background-image: none;
              color: #c30000;
          }
          .carousel-indicators li {
              border-color: #c30000;
          }
          .carousel-indicators li.active {
              background-color: #c30000;
          }
          .item h4 {
              font-size: 19px;
              line-height: 1.375em;
              font-weight: 400;
              font-style: italic;
              margin: 70px 0;
          }
          .item span {
              font-style: normal;
          }
          .panel {
              border: 1px solid #c30000; 
              border-radius:0 !important;
              transition: box-shadow 0.5s;
          }
          .panel:hover {
              box-shadow: 5px 0px 40px rgba(0,0,0, .2);
          }
          .panel-footer .btn:hover {
              border: 1px solid #c30000;
              background-color: #fff !important;
              color: #c30000;
          }
          .panel-heading {
              color: #fff !important;
              background-color: #c30000 !important;
              padding: 25px;
              border-bottom: 1px solid transparent;
              border-top-left-radius: 0px;
              border-top-right-radius: 0px;
              border-bottom-left-radius: 0px;
              border-bottom-right-radius: 0px;
          }
          .panel-footer {
              background-color: white !important;
          }
          .panel-footer h3 {
              font-size: 32px;
          }
          .panel-footer h4 {
              color: #aaa;
              font-size: 14px;
          }
          .panel-footer .btn {
              margin: 15px 0;
              background-color: #c30000;
              color: #fff;
          }
          .navbar {
              margin-bottom: 0;
              background-color: #c30000;
              z-index: 9999;
              border: 0;
              font-size: 12px !important;
              line-height: 1.42857143 !important;
              letter-spacing: 2px;
              border-radius: 0;
              font-family: Montserrat, sans-serif;
          }
          .navbar li a, .navbar .navbar-brand {
              color: #fff !important;
          }
          .navbar-nav li a:hover, .navbar-nav li.active a {
              color: #c30000 !important;
              background-color: #fff !important;
          }
          .navbar-default .navbar-toggle {
              border-color: transparent;
              color: #fff !important;
          }
          footer .glyphicon {
              font-size: 20px;
              margin-bottom: 20px;
              color: #c30000;
          }
          .slideanim {visibility:hidden;}
          .slide {
              animation-name: slide;
              -webkit-animation-name: slide;	
              animation-duration: 1s;	
              -webkit-animation-duration: 1s;
              visibility: visible;			
          }
          @keyframes slide {
            0% {
              opacity: 0;
              -webkit-transform: translateY(70%);
            } 
            100% {
              opacity: 1;
              -webkit-transform: translateY(0%);
            }	
          }
          @-webkit-keyframes slide {
            0% {
              opacity: 0;
              -webkit-transform: translateY(70%);
            } 
            100% {
              opacity: 1;
              -webkit-transform: translateY(0%);
            }
          }
          @media screen and (max-width: 768px) {
            .col-sm-4 {
              text-align: center;
              margin: 25px 0;
            }
            .btn-lg {
                width: 100%;
                margin-bottom: 35px;
            }
          }
          @media screen and (max-width: 480px) {
            .logo {
                font-size: 150px;
            }
          }
          .inner-addon { 
            position: relative; 
        }

        /* style icon */
        .inner-addon .glyphicon {
          position: absolute;
          padding: 10px;
          pointer-events: none;
        }

        /* align icon */
        .left-addon .glyphicon  { left:  0px;}
        .right-addon .glyphicon { right: 0px;}

        /* add padding  */
        .left-addon input  { padding-left:  30px; }
        .right-addon input { padding-right: 30px; }
        
        .modal {
            text-align: center;
            padding: 0!important;
          }

          .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px;
          }

          .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
          }
        
          .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
          .toggle.ios .toggle-handle { border-radius: 20px; }
        
          footer {
            position: fixed;
            bottom: 0px;
            height: 130px;
            width: 100%;
        }
        
        
        
          .login_signup {
            margin: 40px 25px;
            width: 300px;
            display: block;
            border: none;
            padding: 10px 0;
            border-bottom: solid 1px #C30000;
            -webkit-transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 96%, #C30000 4%);
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 96%, #C30000 4%);
            background-position: -300px 0;
            background-size: 300px 100%;
            background-repeat: no-repeat;
            color: #0e6252;
           }

           .login_signup:focus,
             .login_signup:valid {
             box-shadow: none;
             outline: none;
             background-position: 0 0;
           }

           .login_signup::-webkit-input-placeholder {
             font-family: 'roboto', sans-serif;
             -webkit-transition: all 0.3s ease-in-out;
             transition: all 0.3s ease-in-out;
           }

           .login_signup:focus::-webkit-input-placeholder,
             .login_signup:valid::-webkit-input-placeholder {
             color: #C30000;
             font-size: 11px;
             -webkit-transform: translateY(-20px);
             transform: translateY(-20px);
             visibility: visible !important;
           }
           
           .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
           {
                   color: #C30000;
           }
           ul li{
           list-style-type: none;
           display: inline;
           }
           ul li a{
           color: #555;
           }
           ul li a:hover{
           color: #555;
           }


        </style>
        
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBb03ONAydxC2gbsPBSv_DnCkKNCI1nCn8"></script>
        <script type="text/javascript">
            function editProfile() {
                //console.log("here");
                document.getElementById("bank_city").disabled = false;
                document.getElementById("bank_address").disabled = false;
                document.getElementById("bank_location").disabled = false;
                document.getElementById("bank_contact").disabled = false;
                document.getElementById("bank_alternate_contact").disabled = false;
                document.getElementById("bank_email").disabled = false;
                document.getElementById("saveChanges").style.visibility = 'visible';
                document.getElementById("editProfile").style.visibility = 'hidden';
            }
            
            function saveChanges() {
                //console.log("here");
                if(document.getElementById("bank_city").value === "") {
                    document.getElementById("bank_city").focus();
                    return;
                }
                if(document.getElementById("bank_address").value === "") {
                    document.getElementById("bank_address").focus();
                    return;
                }
                if(document.getElementById("bank_location").value === "") {
                    document.getElementById("bank_location").focus();
                    return;
                }
                if(document.getElementById("bank_contact").value === "") {
                    document.getElementById("bank_contact").focus();
                    return;
                }
                if(document.getElementById("bank_alternate_contact").value === "") {
                    document.getElementById("bank_alternate_contact").focus();
                    return;
                }
                if(document.getElementById("bank_email").value === "") {
                    document.getElementById("bank_email").focus();
                    return;
                }
                getLocation();
            }
            
            function getLocation() {
                var geocoder = new google.maps.Geocoder();
                var address = document.getElementById("bank_location").value;
                address = address.concat(", India");
                geocoder.geocode({ 'address': address }, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();
                        updateProfile(latitude, longitude);
                    } 
                    else {
                        alert("Invalid Location.");
                    }
                });
            }
            function updateProfile(latitude, longitude) {
                var bank_email = document.getElementById("bank_email").value;
                var bank_city = document.getElementById("bank_city").value;
                var bank_address = document.getElementById("bank_address").value;
                var bank_location = document.getElementById("bank_location").value;
                var bank_contact = document.getElementById("bank_contact").value;
                var bank_alternate_contact = document.getElementById("bank_alternate_contact").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/UpdateBankProfile";

                var data = new Object();

                data["bank_email"] = bank_email;
                data["bank_city"] = bank_city;
                data["bank_address"] = bank_address;
                data["bank_location"] = bank_location;
                data["bank_contact"] = bank_contact;
                data["bank_alternate_contact"] = bank_alternate_contact;
                data["bank_latitude"] = latitude;
                data["bank_longitude"] = longitude;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "0") {
                                document.getElementById("bank_city").disabled = true;
                                document.getElementById("bank_address").disabled = true;
                                document.getElementById("bank_location").disabled = true;
                                document.getElementById("bank_contact").disabled = true;
                                document.getElementById("bank_alternate_contact").disabled = true;
                                document.getElementById("bank_email").disabled = true;
                                document.getElementById("saveChanges").style.visibility = 'hidden';
                                document.getElementById("editProfile").style.visibility = 'visible';
                            }
                            if(json.return_code === "1048") {
                                $("#myLoginModal").modal('show');
                            }
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
        
            $(function() {
              $('#toggle-event').change(function() {
                    changeStatus($(this).prop('checked'));
              });
            });
            
            
            function changePassword() {
                
                if(document.getElementById("bank_password").value === "") {
                    document.getElementById("bank_password").focus();
                    return;
                }
                if(document.getElementById("bank_confirm_password").value === "") {
                    document.getElementById("bank_confirm_password").focus();
                    return;
                }
                
                var bank_password = document.getElementById("bank_password").value;
                var bank_confirm_password = document.getElementById("bank_confirm_password").value;
                
                if(bank_confirm_password !== bank_password){
                    document.getElementById("bank_confirm_password").focus();
                    return ;
                }
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/ChangeBankPassword";

                var data = new Object();

                data["bank_password"] = bank_password;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "0") {
                                //$("#myModal").hide();
                                $('#myModal').modal('hide');
                                $("#mysecondModal").modal();
                            }
                            else {
                                $('#myModal').modal('hide');
                                $("#myLoginModal").modal('show');
                            }
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
        
        $(document).ready( function () {
            //Search the parent class, id, or tag and then try to find the <a id="addMore"></a>  as a child
            $('#changePassword').on('click', function () {
                changePassword();
            });
        });
        </script>
        <title>Bank Profile</title>
    </head>
    <body>
        
      
        <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
            <a class="navbar-brand" href="#myPage"><span class="glyphicon glyphicon-tint"></span>BloodShare</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href='index.jsp#banks' id="banks"><span class="glyphicon glyphicon-search"></span>BLOOD BANKS</a></li>
            <li><a href="#" id="donors"><span class="glyphicon glyphicon-search"></span>DONORS</a></li>
            <li><a href="index.jsp#request" id="request">REQUEST BLOOD</a></li>
            <li><a href="index.jsp#forum" id="forum"><span class="glyphicon glyphicon-pencil"></span>FORUM</a></li>
            <li><a href="index.jsp#contact" id="contact"><span class="glyphicon glyphicon-earphone"></span>Contact</a></li>
            <li><%
                    if(session.getAttribute("session_name") != null) {
                        if(((String)session.getAttribute("session_type")).equals("user")) out.println("<a href='#'><span class='glyphicon glyphicon-user'></span>"+session.getAttribute("session_name")+"</a>");
                        else out.println("<a href='#'><span class='glyphicon glyphicon-briefcase'></span>Bank_id: "+session.getAttribute("session_name")+"</a>");
                    }
                %>
            </li>
            <li><%
                    if(session.getAttribute("session_name") == null) out.println("<a id='loginlink' href='#'><span class='glyphicon glyphicon-user'></span> Sign Up/Login</a>");
                    else out.println("<a href='login/banks/logoutBank' id='logout'><span class='glyphicon glyphicon-user'></span> Logout</a>");
                %>
            </li>
          </ul>
        </div>
      </div>
    </nav>  
        <%
                    Map bank_data = (HashMap)request.getAttribute("BankData");
        %>
        
        <div id="request" class="container-fluid">
        <h3 style="color:black;" class="text-center"><% out.println("Bank_id: "+bank_data.get("bank_id"));%></h3>
        <h2 style="color:black;" class="text-center"><% out.println(bank_data.get("bank_name"));%></h2>
        <br>
        <div class="row">
              <div class="col-sm-3">
              </div>
              <div class="col-sm-6">
                  <div class="row">
                  <div class="col-sm-6 form-group">
                  </div>
                  <div class="col-sm-6 form-group">
                      <%
                        if(session.getAttribute("session_name") != null) {
                            if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                out.println("<button type='button' id='editProfile' class='btn btn-grey pull-right' onclick='editProfile()'><span class='glyphicon glyphicon-edit'></span> Edit Profile</button>");
                        }
                      %>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                      <label for="text">City:</label>
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                    out.println("<input class='form-control' id='bank_city' name='bank_city' placeholder='City' type='text' disabled value='"+bank_data.get("bank_city")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_city")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_city")+"</font>");
                            
                      %>
                  </div>
                  <div class="col-sm-6 form-group">
                      <label for="text">Address:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                    out.println("<input class='form-control' id='bank_address' name='bank_address' type='text' disabled value='"+bank_data.get("bank_address")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_address")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_address")+"</font>");
                            
                      %>
                  </div>
                </div>
                 <div class="row">
                     
                  <div class="col-sm-6 form-group">
                      <label for="text">Location:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                    out.println("<input class='form-control' id='bank_location' name='bank_location' type='text' disabled value='"+bank_data.get("bank_location")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_location")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_location")+"</font>");
                            
                      %>
                  </div>
                  <div class="col-sm-6 form-group">
                      <label for="text">Contact:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                    out.println("<input class='form-control' id='bank_contact' name='bank_contact' type='text' disabled value='"+bank_data.get("bank_contact")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_contact")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_contact")+"</font>");
                            
                      %>
                  </div>
                </div>
                <div class="row">
                    
                    <div class="col-sm-6 form-group">
                      <label for="text">Alternate Contact:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                    out.println("<input class='form-control' id='bank_alternate_contact' name='bank_alternate_contact' type='text' disabled value='"+bank_data.get("bank_alternate_contact")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_alternate_contact")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_alternate_contact")+"</font>");
                            
                      %>
                </div>
                  <div class="col-sm-6 form-group">
                      <label for="text">Email:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                    out.println("<input class='form-control' id='bank_email' name='bank_email' type='text' disabled value='"+bank_data.get("bank_email")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_email")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+bank_data.get("bank_email")+"</font>");
                      %>
                  </div>
                  
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                      <button id="saveChanges" class="btn btn-primary pull-left" style="visibility: hidden;" onclick="saveChanges()" type="button">Save Changes</button>
                  </div>
                  <div class="col-sm-6 form-group">
                      <%
                        if(session.getAttribute("session_name") != null) {
                            if(((String)session.getAttribute("session_name")).equals((String)(bank_data.get("bank_id"))))
                                out.println("<br><button type='button'  class='btn btn-grey pull-right'  data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-lock'></span> Change Password</button>");
                        }
                      %>
                  </div>
                </div>
              </div>
                  
              <div class="col-sm-3">
              </div>
        </div>
        
    </div>
    <footer class="container-fluid bg-grey text-center">
     
        <p>&copy; Copyright 2016 by Team BloodShare. All Rights Reserved.</p>		
    </footer>
    
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title text-center">Reset Password</h3>
        </div>
        <div class="modal-body">
          <form>
             <label>Password:</label>
             <input type="password" id="bank_password" name="bank_password" class="form-control">
             <label>Confirm Password:</label>
             <input type="password" id="bank_confirm_password" name="bank_confirm_password" class="form-control">
             <br>
             <button type='button' id='changePassword' class='form-control btn-danger'><span class='glyphicon glyphicon-lock'>                    
             </span> Change Password</button>
        </form>        
       </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>

    <script>
        if(document.getElementById("banks"))
        {
            document.getElementById("banks").onclick = function () 
            {
                location.href = "index.jsp#banks";
            };
        }
        if(document.getElementById("donors"))
        {
            document.getElementById("donors").onclick = function () 
            {
                location.href = "index.jsp#donors";
            };
        }
        if(document.getElementById("request"))
        {
            document.getElementById("request").onclick = function () 
            {
                location.href = "index.jsp#request";
            };
        }
        if(document.getElementById("forum"))
        {
            document.getElementById("forum").onclick = function () 
            {
                location.href = "index.jsp#forum";
            };
        }
        if(document.getElementById("contact"))
        {
            document.getElementById("contact").onclick = function () 
            {
                location.href = "index.jsp#contact";
            };
        }

        if(document.getElementById("logout")){
            document.getElementById("logout").onclick = function () {
                location.href = "login/banks/logoutBank";
            };
        }

        if(document.getElementById("loginlink")){
            document.getElementById("loginlink").onclick = function () {
                $("#LoginForm").modal("show");
            };
        }
    </script>
    <div class="modal fade" id="mysecondModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color: #c30000;">Your password has been changed successfully.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
    
    
    <div class="modal fade" id="myLoginModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">You must Login first to complete this action.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
    
    </body>
</html>
