<%-- 
    Document   : error
    Created on : 3 Jan, 2016, 1:28:56 AM
    Author     : shivam_sg
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error Page</title>
    </head>
    <body>
	<font><h1>The page you requested can't be processed.</h1></font>
	<h3>The problem can be</h3>
	<ul type="square">
		<li>Connection can't be made at this time. </li>
		<li>You have entered wrong type of data.</li>
		<li>Check the address for human typing error.</li>
	</ul><br>
    </body>
</html>
