<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>BloodShare</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
  <script type="text/javascript">
            function requestBlood() {
                
                if(document.getElementById("req_name").value === "") {
                    document.getElementById("req_name").focus();
                    return;
                }
                if(document.getElementById("req_email").value === "") {
                    document.getElementById("req_email").focus();
                    return;
                }
                if(document.getElementById("req_phone").value === "") {
                    document.getElementById("req_phone").focus();
                    return;
                }
                if(document.getElementById("req_blood_group").value === "") {
                    document.getElementById("req_blood_group").focus();
                    return;
                }
                if(document.getElementById("req_units").value === "") {
                    document.getElementById("req_units").focus();
                    return;
                }
                var req_name = document.getElementById("req_name").value;
                var req_email = document.getElementById("req_email").value;
                var req_phone = document.getElementById("req_phone").value;
                var req_blood_group = document.getElementById("req_blood_group").value;
                var req_units = document.getElementById("req_units").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/RequestBlood";

                var data = new Object();

                data["req_name"] = req_name;
                data["req_email"] = req_email;
                data["req_phone"] = req_phone;
                data["req_blood_group"] = req_blood_group;
                data["req_units"] = req_units;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            //if(json.return_code === "0")console.log("success");
                            $("#myModal").modal();
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));
            }
            
            function checkUser() {
                
                var user_name = document.getElementById("user_login_name").value;
                
                var user_password = document.getElementById("user_login_password").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "login/users/checkUser";

                var data = new Object();

                data['user_name'] = user_name;
                data['user_password'] = user_password;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "1048") {
                                //console.log("failed");
                                $("#LoginForm").modal('hide');
                                $("#myLoginIncorrectModal").modal('show');
                                //window.open("index.jsp#myLoginIncorrectModal", "_self");
                            }
                            else {
                                window.open("index.jsp", "_self");
                                //console.log("success");
                            }
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
            
            function checkBank() {
                
                var bank_id = document.getElementById("bank_login_name").value;
                
                var bank_password = document.getElementById("bank_login_password").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "login/banks/checkBank";

                var data = new Object();

                data['bank_id'] = bank_id;
                data['bank_password'] = bank_password;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "1048") {
                                //console.log("failed");
                                $("#LoginForm").modal('hide');
                                $("#myLoginIncorrectModal").modal('show');
                                //window.open("index.jsp#myLoginIncorrectModal", "_self");
                            }
                            else {
                                window.open("index.jsp", "_self");
                                //console.log("success");
                            }
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
            function postComment() {
                
                //var comment_user = document.getElementById("comment_user").value;
                var comment_text = document.getElementById("comment_text").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/PostComment";

                var data = new Object();

                //data["comment_user"] = comment_user;
                data["comment_text"] = comment_text;
                //console.log(data["comment_text"]);
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "0")location.reload(true);
                            else $("#myLoginModal").modal('show');
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }
         
           function reportComment(id) {
                //console.log(id);
                var comment_id = id//document.getElementById("comment_id").value;
                //var report_user = document.getElementById("report_user").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/ReportComment";

                var data = new Object();

                data["comment_id"] = comment_id;
                //data["report_user"] = report_user;
                
                var rep = "report_button_";
                rep = rep.concat(comment_id);
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            
                            if(json.return_code === "0") {
                            //console.log("success");
                            document.getElementById(rep).disabled = true;
                            document.getElementById(rep).style.color = "#D43F3A";
                            }
                            else $("#myLoginModal").modal('show');
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
            
            function upvoteComment(id) {
                //console.log(id);
                var comment_id = id;//document.getElementById("comment_id").value;
                //var upvote_user = document.getElementById("upvote_user").value;
                
                xmlhttp=new XMLHttpRequest();
                
                var up = "upvote_button_";
                up = up.concat(comment_id);
                
                if(document.getElementById(up).style.color !== "blue")
                    var url = "admin/services/GiveUpvote";
                else
                    var url = "admin/services/CancelUpvote";
    
                var data = new Object();

                data["comment_id"] = comment_id;
                //data["upvote_user"] = upvote_user;
                
                
                var up_span = "upvote_span_";
                up_span = up_span.concat(comment_id);
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            //console.log("success");
                            if(json.return_code === "0") {
                                if(document.getElementById(up).style.color == "blue") {
                                    document.getElementById(up).style.color = "black";
                                    var upvotes = parseInt(document.getElementById(up_span).innerHTML);
                                    document.getElementById(up_span).innerHTML = --upvotes;
                                }
                                else {
                                    document.getElementById(up).style.color = "blue";
                                    var upvotes = parseInt(document.getElementById(up_span).innerHTML);
                                    document.getElementById(up_span).innerHTML = ++upvotes;
                                }
                            }
                            else $("#myLoginModal").modal('show');
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
            
            function thankDonor(donor_name) {
                //console.log(donor_name);
                var donor_name = donor_name;//document.getElementById("comment_id").value;
                //var upvote_user = document.getElementById("upvote_user").value;
                
                xmlhttp=new XMLHttpRequest();
                
                var tk = "thanks_button_";
                tk = tk.concat(donor_name);
                
                var url = "admin/services/ThanksVote";
                
                var data = new Object();

                data["donor_name"] = donor_name;
                //data["upvote_user"] = upvote_user;
                
                
                var tk_span = "thanks_span_";
                tk_span = tk_span.concat(donor_name);
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            
                            if(json.return_code === "0") {
                                document.getElementById(tk).style.color = "#c30000";
                                document.getElementById(tk).disabled = true;;
                                var thanks_votes = parseInt(document.getElementById(tk_span).innerHTML);
                                document.getElementById(tk_span).innerHTML = ++thanks_votes;
                            }
                            else $("#myLoginModal").modal('show');
                    }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));
            }
            
            function contact() {
                
                if(document.getElementById("name").value === "") {
                    document.getElementById("name").focus();
                    return;
                }
                if(document.getElementById("email").value === "") {
                    document.getElementById("email").focus();
                    return;
                }
                if(document.getElementById("message").value === "") {
                    document.getElementById("message").focus();
                    return;
                }
                
                var name = document.getElementById("name").value;
                var email = document.getElementById("email").value;
                var message = document.getElementById("message").value;
                
                xmlhttp=new XMLHttpRequest();
                
                var url = "admin/services/Contact";
                
                var data = new Object();

                data["name"] = name;
                data["email"] = email;
                data["message"] = message;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            //if(json.return_code === "0") 
                            $("#myContactModal").modal();
                            //else console.log("fail");
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));
            
            }
            
        function getLocation() {
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById("bank_signup_location").value;
            address = address.concat(", India");
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    //alert("Latitude: " + latitude + "\nLongitude: " + longitude);
                    registerBank(latitude, longitude);
                } else {
                    alert("Invalid Location.");
                }
            });
        }
        
        function registerBank(latitude, longitude) {
                var bank_name = document.getElementById("bank_signup_name").value;
                var bank_email = document.getElementById("bank_signup_email").value;
                var bank_city = document.getElementById("bank_signup_city").value;
                var bank_address = document.getElementById("bank_signup_address").value;
                var bank_location = document.getElementById("bank_signup_location").value;
                var bank_contact = document.getElementById("bank_signup_contact").value;
                var bank_alternate_contact = document.getElementById("bank_signup_alternate_contact").value;
                var bank_password = document.getElementById("bank_signup_password").value;
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/RegisterBank";

                var data = new Object();

                data["bank_name"] = bank_name;
                data["bank_email"] = bank_email;
                data["bank_city"] = bank_city;
                data["bank_address"] = bank_address;
                data["bank_location"] = bank_location;
                data["bank_contact"] = bank_contact;
                data["bank_alternate_contact"] = bank_alternate_contact;
                data["bank_password"] = bank_password;
                data["bank_latitude"] = latitude;
                data["bank_longitude"] = longitude;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "1048") {
                                //console.log("failed");
                                $("#LoginForm").modal('hide');
                            }
                            else {
                                $("#LoginForm").modal('hide');
                                $("#mySignedUpModal").modal('show');
                                alert("Your BankId is: "+json.bank_id);
                                window.open("BankProfile?bank="+json.bank_id, "_self");
                            }
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }
        
        canvasCtx = null;
        imageFile = null;
        url = null;
        flag=0;

        window.onload = function () {
                canvasCtx = document.getElementById("signup_panel").getContext("2d");
                document.getElementById("user_signup_picture").onchange = function(event) {

                        flag=1;		
                        this.imageFile = event.target.files[0];

                        var reader = new FileReader();
                        reader.onload =  function(event) {
                                var img = new Image();
                                img.onload = function() {
                                        drawImage(img);
                                };
                                img.src = event.target.result;
                        };
                        reader.readAsDataURL(this.imageFile);
                };

                drawImage = function(img) {
                        this.canvasCtx.canvas.width = 200;
                        this.canvasCtx.canvas.height = 250;
                        this.canvasCtx.drawImage(img,0,0,200,250);
                        url = canvasCtx.canvas.toDataURL("image/png",0.7);
                        //console.log(url);
                        flag=1;
                        setURL(url,flag);
                        //console.log(document.getElementById("hidden").value);
                };
        };

        function setURL(url,flag)
        {
            document.getElementById("signup_hidden").value= url;	
        }

        
        function registerUser() {
                
                var user_name = document.getElementById("user_signup_name").value;
                var user_email = document.getElementById("user_signup_email").value;
                var user_first_name = document.getElementById("user_signup_first_name").value;
                var user_last_name = document.getElementById("user_signup_last_name").value;
                var user_gender = document.getElementById("user_signup_gender").value;
                var user_blood_group = document.getElementById("user_signup_blood_group").value;
                var user_city = document.getElementById("user_signup_city").value;
                var user_location = document.getElementById("user_signup_location").value;
                var user_pic = document.getElementById("signup_hidden").value;
                var user_type_donor = "yes";//document.getElementById("user_signup_type_donor").value;
                var user_available = document.getElementById("user_signup_available").value;
                var user_contact = document.getElementById("user_signup_contact").value;
                var user_alternate_contact = document.getElementById("user_signup_alternate_contact").value;
                var user_password = document.getElementById("user_signup_password").value;
                
                console.log(user_pic);
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/RegisterUser";

                var data = new Object();

                data["user_name"] = user_name;
                data["user_email"] = user_email;
                data["user_first_name"] = user_first_name;
                data["user_last_name"] = user_last_name;
                data["user_gender"] = user_gender;
                data["user_blood_group"] = user_blood_group;
                data["user_city"] = user_city;
                data["user_location"] = user_location;
                data["user_pic"] = user_pic;
                data["user_type_donor"] = user_type_donor;
                data["user_available"] = user_available;
                data["user_contact"] = user_contact;
                data["user_alternate_contact"] = user_alternate_contact;
                data["user_password"] = user_password;

                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "1048") {
                                //console.log("failed");
                                $("#LoginForm").modal('hide');
                                $("#myUserNameModal").modal('show');
                            }
                            else {
                                $("#mySignedUpModal").modal('show');
                                window.open("UserProfile?user="+user_name, "_self");
                            //console.log("success");
                            }
                            //document.getElementById("return_code").value = json.return_code;
                            //document.getElementById("error_string").value = json.error_string;		
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
        }
        
        </script>
        <script src="js/angular.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/map-icons.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src = "js/map.js"></script>
        <script src = "js/infobox.js"></script>
        <!--<script src = "js/infobubble.js"></script>-->
        <script src = "js/map-icons.js"></script>
        <script src = "js/loginform.js"></script>
        <script>
            var banks_json_data;
            
            function bankData() {
            
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/SearchBank";

                xmlhttp.onreadystatechange=function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                banks_json_data = JSON.parse(xmlhttp.responseText);
                                loadMap();
                    }
                }
                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
                xmlhttp.send();	
            }
            
            
            function loadMap() {
                
                //console.log(banks_json_data);
               
                var mapOptions = {
                   center:new google.maps.LatLng(21.5937, 78.9629),
                   zoom:7,
                   panControl: true,
                   zoomControl: true,
                   scaleControl: true,
                   mapTypeControl:true,
                   streetViewControl:true,
                   overviewMapControl:true,
                   rotateControl:true,
                   mapTypeControlOptions: {
                        position:google.maps.ControlPosition.LEFT_BOTTOM,
                   }/*styles: [
			{stylers: [{ visibility: 'simplified' }]},
			{elementType: 'labels', stylers: [{ visibility: 'off' }]}
                   ]*/
                }

                var map = new google.maps.Map(document.getElementById("map"),mapOptions);
                
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        map.setCenter(initialLocation);
                        
                        /*var marker = new google.maps.Marker({
                            position: initialLocation,
                            title: 'Current location',
                            map: map
                        });
                        */
                        
                        var marker = new Marker({
                                map: map,
                                position: initialLocation,
                                icon: {
                                        path: MAP_PIN,
                                        fillColor: '#00CCBB',
                                        fillOpacity: 1,
                                        strokeColor: '',
                                        strokeWeight: 0
                                },
                                //title: 'Current location',
                                map_icon_label: '<span class="map-icon map-icon-postal-code"></span>'
                        });
                        
                        //var infowindow = new google.maps.InfoWindow();
                        
                        var infobox = new InfoBox({
                            content: "<div class='infobox-wrapper'><div id='infobox'style='width:150px; height:40px'><center>Current Location</center></div></div>",
                            disableAutoPan: false,
                            hideCloseBox: true,
                            maxWidth: 150,
                            pixelOffset: new google.maps.Size(-80, 0),
                            zIndex: null,
                            boxStyle: {
                               //background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
                               background: '',
                               opacity: .85,
                               width: "280px"
                           },
                           closeBoxMargin: "12px 4px 2px 2px",
                           //closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                           closeBoxURL: '',
                           infoBoxClearance: new google.maps.Size(1, 1)
                       });
                       /*
                       infoBubble = new InfoBubble({
                        map: map,
                        content: "<div style='color:white;'>Your current location</div>",
                        position: new google.maps.LatLng(-32.0, 149.0),
                        shadowStyle: 1,
                        padding: 0,
                        backgroundColor: '#333',
                        borderRadius: 5,
                        arrowSize: 10,
                        borderWidth: 1,
                        borderColor: 'black',
                        disableAutoPan: true,
                        hideCloseButton: false,
                        arrowPosition: 30,
                        backgroundClassName: 'transparent',
                        arrowStyle: 2
                      });
                      */
                       google.maps.event.addListener(marker, 'mouseover', (function(marker) {
                            return function() {
                              infobox.open(map, marker);
                            }
                        })(marker));
                        
                        google.maps.event.addListener(marker, 'mouseout', function() {
                            infobox.close();
                        });
                        
                        /*google.maps.event.addListener(map, 'click', function() {
                            infobox.close();
                        });*/
                    });
                }
                
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                
                map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

                map.addListener('bounds_changed', function() {
                    searchBox.setBounds(map.getBounds());
                });
   

                var markers = [];
                var searchedPlace = null;
                    searchBox.addListener('places_changed', function() {
                       var places = searchBox.getPlaces();

                       if (places.length == 0) {
                         return;
                       }

                       // Clear out the old markers.
                      /*
                       markers.forEach(function(marker) {
                         marker.setMap(null);
                       });
                       markers = [];
                       */
                       // For each place, get the icon, name and location.
                       var bounds = new google.maps.LatLngBounds();
                       places.forEach(function(place) {
                         
                        /* 
                        var icon = {
                           url: place.icon,
                           size: new google.maps.Size(71, 71),
                           origin: new google.maps.Point(0, 0),
                           anchor: new google.maps.Point(17, 34),
                           scaledSize: new google.maps.Size(25, 25)
                         };

                         markers.push(new google.maps.Marker({
                           map: map,
                           icon: icon,
                           title: place.name,
                           position: place.geometry.location
                         }));
                         */
                            if(searchedPlace != null)
                                searchedPlace.setMap(null);
                       
                            searchedPlace = new google.maps.Circle({
                            center: place.geometry.location,
                            radius: 15000,

                            strokeColor:"#5f84f2",
                            strokeOpacity:0.2,
                            strokeWeight:2,

                            fillColor:"#5f84f2",
                            fillOpacity:0.2
                         });
                         
                         searchedPlace.setMap(map);
                                
                         if (place.geometry.viewport) {
                           bounds.union(place.geometry.viewport);
                         } else {
                           bounds.extend(place.geometry.location);
                         }
                       });
                       map.fitBounds(bounds);
                     });
                     
                var marker, i;
                
                        
                    
                for (i = 0; i < banks_json_data.length; i++) {
                    /*  marker = new google.maps.Marker({
                      position: new google.maps.LatLng(banks_json_data[i]['latlng'][0], banks_json_data[i]['latlng'][1]),
                      title: banks_json_data[i]['name'],
                      map: map,
                      draggable:false,
                      animation:google.maps.Animation.Drop
                    });
                    */
                   infobox = new InfoBox({
                            content: '<div class="infobox-wrapper"><div id="infobox"><div style="background: #00b3a4; border-radius: 2px;"><center><h3>'+banks_json_data[i]['name']+'</h3></center></div><p>City : '+banks_json_data[i]['city']+'<br>Address : '+banks_json_data[i]['address']+'<br>Pincode : '+banks_json_data[i]['location']+'<br>Contact No : '+banks_json_data[i]['contact']+'<br>Alternate No : '+banks_json_data[i]['alternate_contact']+'<br>Email : '+banks_json_data[i]['email']+'</p></div></div>',
                            disableAutoPan: false,
                            maxWidth: 150,
                            pixelOffset: new google.maps.Size(-140, 0),
                            zIndex: null,
                            boxStyle: {
                               background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
                               //background: '',
                               opacity: .85,
                               width: "280px"
                           },
                           closeBoxMargin: "12px 4px 2px 2px",
                           //closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                           closeBoxURL: '',
                           infoBoxClearance: new google.maps.Size(1, 1)
                       });           
                    var marker = new Marker({
                                map: map,
                                position: new google.maps.LatLng(banks_json_data[i]['latlng'][0], banks_json_data[i]['latlng'][1]),
                                icon: {
                                        path: MAP_PIN,
                                        fillColor: '#c30000',
                                        fillOpacity: 1,
                                        strokeColor: '',
                                        strokeWeight: 0
                                },
                                //title: banks_json_data[i]['name'],
                                map_icon_label: '<span class="map-icon map-icon-doctor"></span>',
                                infobox: infobox
                    });

                   
                    /*google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
                      return function() {
                       infobox.setContent(content);
                       infobox.open(map, marker);
                      }
                    })(marker, i));
                    */
                   
                    google.maps.event.addListener(marker, 'mouseover', function() {
                        this.infobox.open(map, this);
                    });
                    
                    google.maps.event.addListener(marker, 'mouseout', function() {
                        this.infobox.close();
                    });    
                    /*google.maps.event.addListener(map, 'click', function() {
                        infobox.close();
                    });*/
                }
            }
            
            google.maps.event.addDomListener(window, 'load', bankData);
            
            $(document).ready(function() {
                if(window.location.href.indexOf('#myLoginModal') != -1) {
                  $('#myLoginModal').modal('show');
                }
            });
            
            $(document).ready(function() {
                if(window.location.href.indexOf('#myLogoutModal') != -1) {
                  $('#myLogoutModal').modal('show');
                }
            });
        
            
            $(document).ready(function() {
                if(window.location.href.indexOf('myLoginIncorrectModal') != -1) {
                  $('#myLoginIncorrectModal').modal('show');
                }
            });
        
            function show() {
                $('#LoginForm').modal('show');
            }
          </script>
          <style>
          body {
              font: 400 15px Lato, sans-serif;
              line-height: 1.8;
              color: #818181;
          }
          
          #map {
              height: 100%;
          }
            
          .controls {
              margin-top: 10px;
              border: 1px solid transparent;
              border-radius: 2px 0 0 2px;
              box-sizing: border-box;
              -moz-box-sizing: border-box;
              height: 32px;
              outline: none;
              box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            }

            #pac-input {
              background-color: #fff;
              font-family: Roboto;
              font-size: 15px;
              font-weight: 300;
              margin-right: 12px;
              padding: 0 11px 0 13px;
              text-overflow: ellipsis;
              width: 300px;
            }

            #pac-input:focus {
              border-color: #4d90fe;
            }

            .pac-container {
              font-family: Roboto;
            }

            #type-selector {
              color: #fff;
              background-color: #4d90fe;
              padding: 5px 11px 0px 11px;
            }

            #type-selector label {
              font-family: Roboto;
              font-size: 13px;
              font-weight: 300;
            }
            #target {
              width: 345px;
            }
            /*.map-icon-label .map-icon {
                    font-size: 12px;
                    color: #000;
                    line-height: 24px;
                    text-align: center;
                    white-space: nowrap;
            }*/
            .infobox-wrapper {
                 border-radius: 15px;
            }
            #infobox {
                border:2px solid black;
                margin-top: 8px;
                background:#333;
                color:#fff;
                font-family: 'Roboto Condensed', sans-serif;
                font-size:15px;
                padding: .5em 1em;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                text-shadow:0 -1px #000000;
                -webkit-box-shadow: 10px 10px 5px #888888;
                box-shadow: 10px 10px 5px #888888;
            }
            .mylabel {
                color:red;
            }
          
            h2 {
              font-size: 24px;
              text-transform: uppercase;
              color: #303030;
              font-weight: 600;
              margin-bottom: 30px;
          }
          h4 {
              font-size: 19px;
              line-height: 1.375em;
              color: #303030;
              font-weight: 400;
              margin-bottom: 30px;
          }  
          .jumbotron {
              background-color: #ff4d4d;
              color: #fff;
              padding: 100px 25px;
              font-family: Montserrat, sans-serif;
              max-height: 300px;
          }
          .container-fluid {
              padding: 60px 50px;
          }
          .bg-grey {
              background-color: #f6f6f6;
          }
          .logo-small {
              color: #c30000;
              font-size: 50px;
          }
          .logo {
              color: #c30000;
              font-size: 200px;
          }
          .thumbnail {
              padding: 0 0 15px 0;
              border: none;
              border-radius: 0;
          }
          .thumbnail img {
              width: 100%;
              height: 100%;
              margin-bottom: 10px;
          }
          .carousel-control.right, .carousel-control.left {
              background-image: none;
              color: #c30000;
          }
          .carousel-indicators li {
              border-color: #c30000;
          }
          .carousel-indicators li.active {
              background-color: #c30000;
          }
          .item h4 {
              font-size: 19px;
              line-height: 1.375em;
              font-weight: 400;
              font-style: italic;
              margin: 70px 0;
          }
          .item span {
              font-style: normal;
          }
          .panel {
              border: 1px solid #c30000; 
              border-radius:0 !important;
              transition: box-shadow 0.5s;
          }
          .panel:hover {
              box-shadow: 5px 0px 40px rgba(0,0,0, .2);
          }
          .panel-footer .btn:hover {
              border: 1px solid #c30000;
              background-color: #fff !important;
              color: #c30000;
          }
          .panel-heading {
              color: #fff !important;
              background-color: #c30000 !important;
              padding: 25px;
              border-bottom: 1px solid transparent;
              border-top-left-radius: 0px;
              border-top-right-radius: 0px;
              border-bottom-left-radius: 0px;
              border-bottom-right-radius: 0px;
          }
          .panel-footer {
              background-color: white !important;
          }
          .panel-footer h3 {
              font-size: 32px;
          }
          .panel-footer h4 {
              color: #aaa;
              font-size: 14px;
          }
          .panel-footer .btn {
              margin: 15px 0;
              background-color: #c30000;
              color: #fff;
          }
          .navbar {
              margin-bottom: 0;
              background-color: #c30000;
              z-index: 9999;
              border: 0;
              font-size: 12px !important;
              line-height: 1.42857143 !important;
              letter-spacing: 2px;
              border-radius: 0;
              font-family: Montserrat, sans-serif;
          }
          .navbar li a, .navbar .navbar-brand {
              color: #fff !important;
          }
          .navbar-nav li a:hover, .navbar-nav li.active a {
              color: #c30000 !important;
              background-color: #fff !important;
          }
          .navbar-default .navbar-toggle {
              border-color: transparent;
              color: #fff !important;
          }
          footer .glyphicon {
              font-size: 20px;
              margin-bottom: 20px;
              color: #c30000;
          }
          .slideanim {visibility:hidden;}
          .slide {
              animation-name: slide;
              -webkit-animation-name: slide;	
              animation-duration: 1s;	
              -webkit-animation-duration: 1s;
              visibility: visible;			
          }
          @keyframes slide {
            0% {
              opacity: 0;
              -webkit-transform: translateY(70%);
            } 
            100% {
              opacity: 1;
              -webkit-transform: translateY(0%);
            }	
          }
          @-webkit-keyframes slide {
            0% {
              opacity: 0;
              -webkit-transform: translateY(70%);
            } 
            100% {
              opacity: 1;
              -webkit-transform: translateY(0%);
            }
          }
          @media screen and (max-width: 768px) {
            .col-sm-4 {
              text-align: center;
              margin: 25px 0;
            }
            .btn-lg {
                width: 100%;
                margin-bottom: 35px;
            }
          }
          @media screen and (max-width: 480px) {
            .logo {
                font-size: 150px;
            }
          }
          .inner-addon { 
            position: relative; 
        }

        /* style icon */
        .inner-addon .glyphicon {
          position: absolute;
          padding: 10px;
          pointer-events: none;
        }

        /* align icon */
        .left-addon .glyphicon  { left:  0px;}
        .right-addon .glyphicon { right: 0px;}

        /* add padding  */
        .left-addon input  { padding-left:  30px; }
        .right-addon input { padding-right: 30px; }
        
        .modal {
            text-align: center;
            padding: 0!important;
          }

          .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px;
          }

          .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
          }
          
          
          
          .login_signup {
            margin: 40px 25px;
            width: 300px;
            display: block;
            border: none;
            padding: 10px 0;
            border-bottom: solid 1px #C30000;
            -webkit-transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 96%, #C30000 4%);
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 96%, #C30000 4%);
            background-position: -300px 0;
            background-size: 300px 100%;
            background-repeat: no-repeat;
            color: #0e6252;
           }

           .login_signup:focus,
             .login_signup:valid {
             box-shadow: none;
             outline: none;
             background-position: 0 0;
           }

           .login_signup::-webkit-input-placeholder {
             font-family: 'roboto', sans-serif;
             -webkit-transition: all 0.3s ease-in-out;
             transition: all 0.3s ease-in-out;
           }

           .login_signup:focus::-webkit-input-placeholder,
             .login_signup:valid::-webkit-input-placeholder {
             color: #C30000;
             font-size: 11px;
             -webkit-transform: translateY(-20px);
             transform: translateY(-20px);
             visibility: visible !important;
           }
           
           .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
           {
                   color: #C30000;
           }
           ul li{
           list-style-type: none;
           display: inline;
           }
           ul li a{
           color: #555;
           }
           ul li a:hover{
           color: #555;
           }

        </style>
    </head>
    <body onload="Login123()" id="myPage" ng-app="indexPage" data-spy="scroll" data-target=".navbar" data-offset="60">

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="#myPage"><span class="glyphicon glyphicon-tint"></span>BloodShare</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#banks"><span class="glyphicon glyphicon-search"></span>BLOOD BANKS</a></li>
            <li><a href="#donors"><span class="glyphicon glyphicon-search"></span>DONORS</a></li>
            <li><a href="#request">REQUEST BLOOD</a></li>
            <li><a href="#forum"><span class="glyphicon glyphicon-pencil"></span>FORUM</a></li>
            <li><a href="#contact"><span class="glyphicon glyphicon-earphone"></span>Contact</a></li>
            <li><%
                    if(session.getAttribute("session_name") != null) {
                        if(((String)session.getAttribute("session_type")).equals("user")) out.println("<a id='profile' href='UserProfile?user="+session.getAttribute("session_name")+"'><span class='glyphicon glyphicon-user'></span>"+session.getAttribute("session_name")+"</a>");
                        else out.println("<a id='profile' href='BankProfile?bank="+session.getAttribute("session_name")+"'><span class='glyphicon glyphicon-briefcase'></span>Bank_id: "+session.getAttribute("session_name")+"</a>");
                    }
                %>
            </li>
            <li><%
                    if(session.getAttribute("session_name") == null) out.println("<a id='loginlink' href='#'><span class='glyphicon glyphicon-user'></span> Sign Up/Login</a>");
                    else out.println("<a href='login/banks/logoutBank' id='logout'><span class='glyphicon glyphicon-user'></span> Logout</a>");
                %>
            </li>
          </ul>
        </div>
      </div>
    </nav>
      
    <div class="jumbotron text-center">
      <h1>BloodShare</h1>
      <p>#Donate_Blood_Save_Life</p> 
    </div>

    <!-- Container (About Section) -->
    <div id="banks" class="container-fluid">
        <div class="container">
            <input id="pac-input" class="controls" type="text" placeholder="Search locations for Blood banks">
            <div id = "map" style="height: 350px; width: auto;"></div>
        </div>
    </div>

    <div id="donors" class="container-fluid bg-grey">
        <div class="row">
            <div class="col-sm-3">
              <span class="glyphicon glyphicon-tint logo slideanim"></span>
            </div>
            <div class="col-sm-9">
                <h2>&nbsp;&nbsp;&nbsp;Find Donors</h2><br>
                <div class="row">
                  <div class="col-sm-4 slideanim">
                      <div class="inner-addon left-addon">
                      <i class="glyphicon glyphicon-tint"></i>
                      <input type="text" class="form-control" ng-model="user_blood_group" id="user_blood_group" name="user_blood_group" placeholder="blood group">
                      </div>
                    </div>
                    <div class="col-sm-4 slideanim">
                      <div class="inner-addon left-addon">
                      <i class="glyphicon glyphicon-globe"></i>
                      <input type="text" class="form-control" ng-model="user_city" id="user_city" name="user_city" placeholder="city">
                      </div>
                    </div>
                    <div class="col-sm-4 slideanim">
                      <div class="inner-addon left-addon">
                      <i class="glyphicon glyphicon-map-marker"></i>
                      <input type="text" class="form-control" ng-model="user_location" id="user_location" name="user_location" placeholder="location">
                      </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div ng-controller="UserSearchController">
                        <ul ng-show=" user_blood_group || user_city || user_location">
                            <li style="list-style-type:none;" ng-repeat="user in users | searchUserBloodGroup:user_blood_group | searchUserCity:user_city | searchUserLocation:user_location">
                                <p><img ng-src="{{user.pic}}" class="img-circle" height="60" width="60">&nbsp;&nbsp;<b><font style="color:black;"><a ng-href="UserProfile?user={{user.name}}"  style="text-decoration: none;">{{user.first_name}} {{user.last_name}}</a></font>&nbsp;&nbsp;&nbsp;<button
                                            ng-disabled="user.thanks_flag == '#c30000' || user.name == user.current_user || user.current_user == null || user.thanks_flag_show != 1" id="thanks_button_{{user.name}}" style = "color : {{user.thanks_flag}};  background-color: #F1F1F1; border: none; text-align: center; text-decoration: none; display: inline-block; font-size: 10 px;" onclick="thankDonor('{{user.name}}')"><span class="glyphicon glyphicon-star-empty"></span></button></b><span id="thanks_span_{{user.name}}">{{user.thanks_vote}}</span> <br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-tint"></span>&nbsp;{{user.blood_group}}&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-globe"></span>&nbsp;{{user.city}}&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-map-marker"></span>&nbsp;{{user.location}}&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-phone"></span>&nbsp;{{user.contact}}&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-earphone"></span>&nbsp;{{user.alternate_contact}}&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-envelope"></span>&nbsp;{{user.email}}<br>
                                </p><br>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div id="request" class="container-fluid">
        <h2  class="text-center">Request for Blood</h2>
        <br>
        <div class="row">
              <div class="col-sm-5">
                <p>Can't find any donor...? <br>We'll be happy to help you.<br>   
                   Please provide us the details regarding your blood requirement, we'll get back to you within 24 hours.</p>
                	   
              </div>
              <div class="col-sm-7 slideanim">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <input class="form-control" id="req_name" name="req_name" placeholder="Name" type="text" required>
                  </div>
                  <div class="col-sm-6 form-group">
                    <input class="form-control" id="req_email" name="req_email" placeholder="Email" type="email" required>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <input class="form-control" id="req_phone" name="req_phone" placeholder="Phone" type="text" required>
                  </div>
                  <div class="col-sm-6 form-group">
                    <select class="form-control" id="req_blood_group" name="req_blood_group">
                        <option value="A+">A+</option>
                        <option value="A-">A-</option>
                        <option value="A1+">A1+</option>
                        <option value="A1-">A1-</option>
                        <option value="A1B+">A1B+</option>
                        <option value="A1B-">A1B-</option>
                        <option value="A2+">A2+</option>
                        <option value="A2-">A2-</option>
                        <option value="A2B+">A2B+</option>
                        <option value="A2B-">A2B-</option>
                        <option value="AB+">AB+</option>
                        <option value="AB-">AB-</option>
                        <option value="B+">B+</option>
                        <option value="B-">B-</option>
                        <option value="HH">HH</option>
                        <option value="O+">O+</option>
                        <option value="O-">O-</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <input class="form-control" id="req_units" name="req_units" placeholder="No. of units required" type="text" required>
                  </div>
                </div>	<div class="row">
                  <div class="col-sm-6 form-group">
                    <button class="btn btn-primary pull-left" onclick="requestBlood()" type="button">Submit</button>
                  </div>
                </div>	
              </div>
        </div>
    </div>


    <!-- Container (Contact Section) -->
    <div id="forum" class="container-fluid bg-grey">
      <h2 class="text-center">Forum</h2>
      <div class="row" ng-controller="GetCommentController">
            <div class="col-sm-1 form-group">
            </div>
            <div class="col-sm-10 form-group pull-left">
                <ul>
                    <li ng-show="comment.index <= current && comment.index > current-5 "  ng-repeat="comment in comments" style="list-style-type:none;">
                 
                        <p><img ng-src="{{comment.pic}}" class="img-circle" height="60" width="60">&nbsp;&nbsp;<b><font style="color:black;"><a style="text-decoration: none;" ng-href="UserProfile?user={{comment.user}}">{{comment.user}}</a></font></b><br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font style="color:#333">{{comment.text}}</font><br> 
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button <%
                if(session.getAttribute("session_name") == null || ((String)session.getAttribute("session_type")).equals("bank")) out.println("disabled");%> id="upvote_button_{{comment.id}}" style = "color : {{comment.up_flag}}; background-color: #F1F1F1; border: none; text-align: center; text-decoration: none; display: inline-block; font-size: 10 px;" onclick="upvoteComment({{comment.id}})"><span class="glyphicon glyphicon-thumbs-up"></span></button><span id="upvote_span_{{comment.id}}">{{comment.upvote}}</span> &nbsp;&nbsp;&nbsp;{{comment.created}}&nbsp;&nbsp;&nbsp;<%
                if(session.getAttribute("session_name") != null && (!((String)session.getAttribute("session_type")).equals("bank"))) out.println("<button ng-disabled='comment.rep_flag_show == 1'   id = 'report_button_{{comment.id}}' style='color : {{comment.rep_flag}}; background-color: #F1F1F1; border: none; text-align: center; text-decoration: none; display: inline-block;' onclick='reportComment({{comment.id}})'>Report</button>");%></p>
                    </li>
                    
                    <li style="list-style-type:none;">
                        <br><br>
                        <a href="" style="text-decoration: none" ng-click="previous()" ng-hide="current-5<0">
                            <span class="glyphicon glyphicon-chevron-left"></span> Previous
                        </a><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <a href="" style="text-decoration: none" ng-click="next()" ng-hide="current+1>=len">
                            <span class="glyphicon glyphicon-chevron-right"></span> Next
                        </a>
                        <br><br>
                    </li>
                        <%
                            if(session.getAttribute("session_name") != null && (!((String)session.getAttribute("session_type")).equals("bank"))) out.println("<textarea class='form-control' id='comment_text' name='comment_text' placeholder='Comment here' rows='5'></textarea><br><div class='row'><div class='col-sm-12 form-group'><button id='comment_button' class='btn btn-primary' onclick='postComment()' type='button'>Submit</button></div></div>");
                        %>
                </ul>
            </div>
            <div class="col-sm-1 form-group">
            </div>
      </div>
    </div>

    <!-- Container (Contact Section) -->
    <div id="contact" class="container-fluid">
      <h2 class="text-center">CONTACT</h2>
      <div class="row">
        <div class="col-sm-5">
          <p>Contact us and we'll get back to you within 24 hours.</p>
          <p><span class="glyphicon glyphicon-globe"></span> Allahabad, India</p>
          <p><span class="glyphicon glyphicon-phone"></span> +91 9876543210</p>
          <p><span class="glyphicon glyphicon-envelope"></span> support@bloodshare.org</p>	   
        </div>
        <div class="col-sm-7 slideanim">
          <div class="row">
            <div class="col-sm-6 form-group">
              <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
            </div>
            <div class="col-sm-6 form-group">
              <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
            </div>
          </div>
          <textarea class="form-control" id="message" name="message" placeholder="Comment/Suggestion" rows="5"></textarea><br>
          <div class="row">
            <div class="col-sm-12 form-group">
                <button class="btn btn-primary" type="button" onclick="contact()">Submit</button>
            </div>
          </div>	
        </div>
      </div>
    </div>

    
    <footer class="container-fluid bg-grey text-center">
      <a href="#myPage" title="To Top">
        <span class="glyphicon glyphicon-chevron-up"></span>
      </a>
        <p>&copy; Copyright 2016 by Team BloodShare. All Rights Reserved.</p>		
    </footer>

    <script>
    $(document).ready(function(){
      // Add smooth scrolling to all links in navbar + footer link
      $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

        // Prevent default anchor click behavior
        event.preventDefault();

        // Store hash
        var hash = this.hash;

        // Using jQuery's animate() method to add smooth page scroll
        // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 900, function(){

          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        });
      });

      $(window).scroll(function() {
        $(".slideanim").each(function(){
          var pos = $(this).offset().top;

          var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
              $(this).addClass("slide");
            }
        });
      });
    })
    
    if(document.getElementById("logout")){
                document.getElementById("logout").onclick = function () {
        location.href = "login/banks/logoutBank";
        };
    }
    
    if(document.getElementById("loginlink")){
                document.getElementById("loginlink").onclick = function () {
                    $("#LoginForm").modal("show");
        };
    }
    </script>

    <script src="index.script.js"></script>
        
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">Your request has been registered.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div><div class="modal fade" id="myContactModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">We have received your comment/suggestion. Thanks for contacting us!</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
      <div class="modal fade" id="myLoginModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">You must Login first to complete this action.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
      <div class="modal fade" id="myLogoutModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">You have been successfully Logged out.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
      <div class="modal fade" id="myLoginIncorrectModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">Your credentials are incorrect. Please try again.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
      <div class="modal fade" id="mySignedUpModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #5cb85c;">Welcome to BloodShare! you have registered successfully.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
      <div class="modal fade" id="myUserNameModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">Uesrname not available. Please try with any other username.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
    
    
    
    
    <div class="container"><center> <div class="row">
    <div class="modal fade" id="LoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         
            <span><input  class="login_signup" type="button" value="Login" onclick="Login123()" style="font: Arial;display: inline;width:200px;color:#b3b3b3;"></span>
			<span><input  class="login_signup" type="button" value="SignUp" onclick="SignUp123()" style="font: Arial;display: inline; width:200px;color:#b3b3b3;"></span>
      </div>
      <div class="modal-body">
         <div id="lg123456">
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#home">User Login</a></li>
				<li><a data-toggle="tab" href="#page" >Blood Bank Login</a></li>
			
		
			</ul>
			<div class="tab-content">
				
				<div id="home" class="tab-pane fade in active">
                                    <form><center>
					<div id="user_login_nameA" style="color:green;"></div>
					<div>
						
						<input  class="login_signup" type="text" id="user_login_name" placeholder="Enter Username" required>
					</div>
					<div id="user_login_nameB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup" type="password" id="user_login_password" placeholder="Enter Password" required>
					</div>
					<div id="user_login_passwordB" style="color:red;" ></div>
					<br>
					<button class="btn btn-danger" type="button" onclick="check_user_login('user_login_name','user_login_password')"><span class="glyphicon glyphicon-lock"></span>&nbsp; Secure Login</button>
                                        </center></form>
				</div>
				
				<div id="page" class="tab-pane fade">
				
				<form>
                                    <center>
					<div id="bank_login_nameA" style="color:green;"></div>
					<div>
						
						<input  class="login_signup"  type="text" id="bank_login_name" placeholder="Enter BankId" required>
					</div>
					<div id="bank_login_nameB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="password" id="bank_login_password" placeholder="Enter Password" required>
					</div>
					<div id="bank_login_passwordB" style="color:red;" ></div>
					<br>
					<button class="btn btn-danger" type="button" onclick="check_bank_login('bank_login_name','bank_login_password')"><span class="glyphicon glyphicon-lock"></span>&nbsp; Secure Login</button>
                                    </center>       
				</form>
				
				</div>  
				
				
			
			</div>
		</div>	
		<div id="sg123456">
				
			     
			<ul class="nav nav-tabs nav-justified">

				<li class="active"><a data-toggle="tab" href="#home1"> Blood Bank SignUp</a></li>
				<li><a data-toggle="tab" href="#page1">User SignUp</a></li>
		<!--	<li><a data-toggle="tab" href="#page">Blood Bank Login</a></li> -->
			
		
			</ul>
			<div class="tab-content">
				
				<div id="home1" class="tab-pane fade in active">
				<form  name="bank_reg">
                                    <center>
					<div id="succ_submit" style="color:green;"></div>
					<div>
						<input  class="login_signup"  type="text" id="bank_signup_name" placeholder="Enter Name" required>
					</div>
					<div id="bank_signup_nameB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="email" id="bank_signup_email" placeholder="Enter Email" onkeyup="Submit('bank_signup_name')" required>
					</div>
					<div id="bank_signup_emailB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="text" id="bank_signup_city" placeholder="Enter City" onkeyup="Submit('bank_signup_name','bank_signup_email')" required>
					</div>
					<div id="bank_signup_cityB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="text" id="bank_signup_location" placeholder="Enter Location" onkeypress="return isNumber(event)" onkeyup="Submit('bank_signup_name','bank_signup_email','bank_signup_city')" required>
					</div>
					<div id="bank_signup_locationB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="text" id="bank_signup_contact" placeholder="Enter Contact" onkeypress="return isNumber(event)" onkeyup="Submit('bank_signup_name','bank_signup_email','bank_signup_city','bank_signup_location')" required>
						
					</div>
					<div id="bank_signup_contactB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="text" id="bank_signup_alternate_contact" placeholder="Enter Alternate Contact" onkeypress="return isNumber(event)" onkeyup="Submit('bank_signup_name','bank_signup_email','bank_signup_city','bank_signup_location','bank_signup_contact')" required>
					</div>
					<div id="bank_signup_alternate_contactB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="password" id="bank_signup_password" placeholder="Enter password" onkeyup="Submit('bank_signup_name','bank_signup_email','bank_signup_city','bank_signup_location','bank_signup_contact','bank_signup_alternate_contact')" required>
					</div>
					<div id="bank_signup_passwordB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="password" id="bank_signup_confirm_password" placeholder="Confirm password"  onkeyup="Submit('bank_signup_name','bank_signup_email','bank_signup_city','bank_signup_location','bank_signup_contact','bank_signup_alternate_contact','bank_signup_password')" required>
					</div>
					<div id="bank_signup_confirm_passwordB" style="color:red;" ></div>
					<div>
						
						<input  class="login_signup"  type="text" id="bank_signup_address" placeholder="Enter Address"  onkeyup="Submit('bank_signup_name','bank_signup_email','bank_signup_city','bank_signup_location','bank_signup_contact','bank_signup_alternate_contact','bank_signup_password','bank_signup_confirm_password')" required>
					</div>
					<div id="bank_signup_addressB" style="color:red;" ></div>
					<button type="button" class="btn btn-danger" onclick="Submit('bank_signup_name','bank_signup_email','bank_signup_city','bank_signup_location','bank_signup_contact','bank_signup_alternate_contact','bank_signup_password','bank_signup_confirm_password','bank_signup_address')"><span class="glyphicon glyphicon-lock"></span>&nbsp; Secure SignUp</button><br>
					<br>
					 
                                    </center>
				</form>
				</div>
			<div id="page1" class="tab-pane fade">
					<form>
                                            <center>
					<div id="succ_submit1" style="color:green;"></div>
					<table>	
					<tr>
						
						<td><input   class="login_signup" type="text"  id="user_signup_name" placeholder="Enter User Name" style="display:inline;width:200px;background-position: -200px 0; background-size: 200px 100%;" required></td>
						
						<td><input  class="login_signup"  type="email" id="user_signup_email" placeholder="Enter Email" style="display:inline;width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeyup="Submit1('user_signup_name')" required></td>
					</tr>
					<tr>
						
							<td id="user_signup_nameB" style="color:red;text-align:center;"></td>
					
							<td id="user_signup_emailB" style="color:red;text-align:center;"></td>
					
					</tr>
					
				
					<tr>
					
						<td><center> <input  class="login_signup"  type="text" id="user_signup_first_name" placeholder="Enter First Name" style="width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeyup="Submit1('user_signup_name','user_signup_email')" required></center></td>
				
						<td><center>	<input   class="login_signup" type="text"  id="user_signup_last_name" placeholder="Enter Last Name" style="width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeyup="Submit1('user_signup_name','user_signup_email','user_signup_first_name')" required></center></td>
					</tr>
					<tr>
							<td id="user_signup_first_nameB" style="color:red;text-align:center;"></td>
							<td id="user_signup_last_nameB" style="color:red;text-align:center;"></td>
					</tr>
					
					<tr>
					<td class="form-group"><center>
						<label style="font: 14px Arial;color: #b3b3b3;" for="text" >User Gender:</label>
						<select style="color: #C30000;" class="form-group" id="user_signup_gender" >
 							<option value="Male">Male</option>
 							<option value="Female">Female</option>
						   
						</select>
						</center>
					</td>
				
					<td class="form-group"><center>
						<label style="font: 14px Arial;color: #b3b3b3;" for="text" >User Blood Group:</label>
						<select style="color: #C30000;" class="form-group" id="user_signup_blood_group" >
 							<option value="saab">A+</option>
 							<option value="mercedes">A-</option>
						    <option value="audi">B+</option>
							<option value="volvo">B-</option>
							<option value="saab">AB+</option>
							<option value="mercedes">AB-</option>
							<option value="audi">O+</option>
							<option value="audi">O-</option>


						</select>
						</center>
					</td>
					</tr>
					<br>
					
					
					<tr>
						
					<td><center>	<input   class="login_signup" type="text"  id="user_signup_city" placeholder="Enter City" style="width:200px;background-position: -200px 0; background-size: 200px 100%;"onkeyup="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name')" required></center></td>
					
						
					<td><center>	<input   class="login_signup" type="text"  id="user_signup_location" placeholder="Enter Pin Code" style="width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeypress="return isNumber1(event)" onkeyup="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name','user_signup_city')" required></center></td>
					</tr>
					<tr>
							<td id="user_signup_cityB" style="color:red;text-align:center;"></td>
							<td id="user_signup_locationB" style="color:red;text-align:center;"></td>
					</tr>
					
					<tr>
						
					<td><center>	<input  class="login_signup"  type="text" id="user_signup_contact" placeholder="Enter Contact" style="width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeypress="return isNumber1(event)" onkeyup="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name','user_signup_city','user_signup_location')" required></center></td>
						
					<td><center>	<input   class="login_signup" type="text" id="user_signup_alternate_contact" placeholder="Enter Alternate Contact" style="width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeypress="return isNumber1(event)" onkeyup="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name','user_signup_city','user_signup_location','user_signup_contact')" required></center></td>
					</tr>
					<tr>
							<td id="user_signup_contactB" style="color:red;text-align:center;"></td>
							<td id="user_signup_alternate_contactB" style="color:red;text-align:center;"></td>
					</tr>
					
					<tr>
						
					<td><center>	<input   class="login_signup" type="password" id="user_signup_password" placeholder="Enter password" style="width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeyup="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name','user_signup_city','user_signup_location','user_signup_contact','user_signup_alternate_contact')" required></center></td>
						
					<td><center>	<input   class="login_signup" type="password" id="user_signup_confirm_password" placeholder="Confirm password" style="width:200px;background-position: -200px 0; background-size: 200px 100%;" onkeyup="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name','user_signup_city','user_signup_location','user_signup_contact','user_signup_alternate_contact','user_signup_password')" required></center></td>
					</tr>
					<tr>
							<td id="user_signup_passwordB" style="color:red;text-align:center;"></td>
							<td id="user_signup_confirm_passwordB" style="color:red;text-align:center;"></td>
					</tr>
					
					<tr>
						
					<td><center>	<input  class="login_signup"  type="file" accept="image/*" id="user_signup_picture" placeholder="Upload Pic" onclick="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name','user_signup_city','user_signup_location','user_signup_contact','user_signup_alternate_contact','user_signup_password','user_signup_confirm_password')" required></center> </td>
                                        <td><canvas id="signup_panel" height="250" width="200"></canvas></td>
                                        </tr>
					<tr>
							<td colspan="2" id="user_signup_pictureB" style="color:red;text-align:center;"></td>
							
					</tr>
					
					
					</table>
					<button class="btn btn-danger" type="button" onclick="Submit1('user_signup_name','user_signup_email','user_signup_first_name','user_signup_last_name','user_signup_city','user_signup_location','user_signup_contact','user_signup_alternate_contact','user_signup_password','user_signup_confirm_password')"><span class="glyphicon glyphicon-lock"></span>&nbsp; Secure SignUp</button>
					
					
                                        <input type="hidden" id="signup_hidden" value="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAUDBAQEAwUEBAQFBQUGBwwIBwcHBw8LCwkMEQ8SEhEPERETFhwXExQaFRERGCEYGh0dHx8fExciJCIeJBweHx7/2wBDAQUFBQcGBw4ICA4eFBEUHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh7/wAARCAEsASwDASIAAhEBAxEB/8QAGwABAQEBAQEBAQAAAAAAAAAAAAECBQMEBgj/xAA2EAEAAgIBAwMBBgMGBwAAAAAAARECAwQhMVEFEkFhBhMicYGRIzLwFDNCocHhFSRDUnKS0f/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD+ywAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAS5LQqAasQBRAFuPJcIAogClwgCiAKXHlAFEAUQBRAFuC48oAogCiAKIAogCiAKIAogDIAHY7gAAAWAAAFlyAAAB2ADuBYBcvDZzOLrynHZv145R8TJr5fG2Ze3DfrynxGQPfuAAWRNgFyWAEdJJ6yAFgAtx5LjygC3HlJnwAFysT0QBbjyXHlAAS5LkFEuS5BRLlAaGVuQUS5LkFEuS5BRJmUBqejz379WjXOe3PHGK+Z7/AJPl9S5+vh41cZbJ7YRPX9X5vlcnfytk57s5mfjH4j8oB1Ob63nllOPEwjHGq9+cdf0c3dy+Vtv7zkbMombq+zwUEmP3Iipv58lfVQemrkcjV/db9mPW/wCZ0NHrfIxmI3a8NmPzMdJcsB+gw9d40zWerbjHnpL7+LytHJwnLTsjOI7+Yfj66vbicjbxd0bNWVTHeL6ZR4kH7EeHF3Yb9OO7CqmI6XcxPiXtPUFGSOgNDKxMgolyXIKJcp3BoZWJkFGQEuS5AC5LkALkuQAuS5AC5LkALksSoBqXx+p83DiaO97J6Yx/q+nZnGGvLPKaxxiZl+T53Iy5PKy3ZRMX0iPEeAeWzPPbsnZsynLKeszKfKp8gR9VAAAAAAAHtxOVu4ucZas8qu5xvpk/S8Dm6eZheuayjvjPeP68vyj042/Pj78d2E9cZ6/WAfsB5aNuO7Vjtwm8couHoClwgC3BKALcf1JCAKXCAKIAlx5LjygC3HkuPKALceS48oAtx5LjygC3HkuPKALceS4QByvtFyPZpw42PfOby/KP93CfX6zt+99R2TFTGNYft3/zfKAk9gAhQAAAABA8kdgVJUB3fs3t92jZpnK5wyuI+kurcOD9nJrlbI84f6u7+tg0M9TqC3CsnUGkuPKANJcJ1AaS48p1ABLkuQWZpL+h3AWOokdC5BZSJJmQFEuS5BUsuUmLv6g/H55TlnllM3llMzJJlE47MsZrpM9fJIBAoAAAAAEdQA+QBD9SQdT7ORM8nZMTVYf6u9DjfZvX+Hdu+ZmMXYuQUS5LkFEmZQGhLkuQUS5LkFGVuQBkBoZAaGQGhkBoZAaGQH5f1HX91zd2HaPdcfk8X2+tbtO7lRlpn3TjHtyy+Jm3xACSQCgAAAAgKCAVBP5Wr6PTdUbubrwy7Xc/p1B3/TdP9n4evXNTlVzMfV9LEeIpQaGQGhkBoZui7BoZssGhkBLjyXHlAFuPJceUAW48lx5QBbjyXHlAFuPJceUAW48vPkROXH244xeU4TEftLYD8j8fVY7PXm444czdhhMTjGf9Q8wBL6qAAAAARIAfJExQgL8PXhY5ZcvVGE1Pu808pm33+hY45c3LKYuccJnH9wd+Z+UuPKALceS48oAtx5LjygC3BcIAtx5LjygC3HkuPKAAlyXIKJclyCiXJcgolyXIKJclyCiXJcg/OeqastXO2X2yy90T+fd83zT9B6rxf7TovCP4mPXH6x4cDPDPDOs8csZ+YmKoE7TZE2TFkRQKAAAAABHUI6AFOj9nsJnk55/9uFfvMPh0atu+Zx1YTnMd4h3vTONPG0e3KYnZlN5V8fQH1gACTPVY7AAAAAAABcAM3CsgNDIDQyA0MgNDIDQyArk/aGIvRlX4puL/AEdWfzcz1/Xllow2xH8k9evkHJEhQBJ/QgFAAAAIjoAOt9n4/hbcvMxDqU+H0bGMeDGUR/NMz/m+0GrkuWQGjqyA1clyyA1clyyA1clyyA0XLIAM3K2ClpZPWQW48iET0BRnqtgol+SwUSywV5czV99x89fS5jpfl6WTIPy1V0V7+oaJ0cvKP8OX4sZeAIqfuAoAAACK9eDq+95evCri7n8gd/i4fd8bXhVTGMRMeOj1SywUSywUSywUSywUSywUSywUSywBm5J6g0MkdAaGQGhknqDQz2LkGhm5LkFGcs4xxnLKaxjy5nN9S74ceek9Pf8A/AT1rfhnnjpx6zhNzLnJ1mblQQUAAAAAdH0XZpxnLDLpty7TPzH0c5OsdpB+oVz/AE7mY7cMde3P+LHaZ/xQ+6ZsGhnsXINDNyXINDNyXINDNyXINDNyXINDNyXIAAAJIKEAAlx/UM7NmGuLzzxwj6yDY+TP1Di4/wDV93/jFvn5HqlXGjX+uQOjnnhhE5Z5RjEeXw8j1PVjFaY+8nzPSIcvbt2bcvdsznKWAenI5G3fl+POZjx8POutlQoAAAAAAAR3J7gAAnbq+zj+o7tf4cpjbEeZ6w+M+Ad/i8zRyIrHKYy+Yy+H0PzGPSpuqfXxvUN+qPbnMbI+vcHcHOw9V0zNZas4/WJen/EeN5z/APUH2jx18nRnUYbsJmfi+71iQUAAAAAGbguEAW4LhEBroxszwwwnPPL24x36Mcjfho1znnPT4jy4vK37ORn7s5/D8Y/Ef7g+vleo55T7dEe2PnKe74M8888vdnlMz9QARQAAAAACJoAPkiegATPQAAAAAAAAARQEns9+Py9+moxzvGOlZdYeIDtcTna934Zj2Z+Jnu+rv9X5qbdLgc6YmNW7LLL4jKZB06+gl+CwULLBAAGdmWOGGWeU1jjFzLTmes7b9umMvrlEfIPj5W7LfunPLt8R4h5CghYUCgAAAAfIAdgCOoR0AAAAAAAAAAAAACO4AZfCVa9zsDp+mcn3Yxoz/njtPl0I7PzmGeWGcZYzWUdYd/Rsjbqx2Yz0kHoADNyXKAGU1E5TPSOrgbc52bcs5+ZmXX9Rz9nDz79ajo43yAT0hUBQAAAAAAAO/cnpIAAAAAAAAAAAAAAAAAARAI6Xo+zpnqmfrHRzvl7+nZezl4eJ6T1oHauS5AEBJB8fq0/8tEecnLj6uj6v/d4R9XO+QVFT4BQAAAAAAAAAAAAAAAAAAAAAAAAAEUAa48+3frmeke6J/wA2UmfbUx3B+gExmZiJ8w0D/9k=" name="hidden">
                                            </center>
				</form>
			</div>
				
	<!--		<div id="page" class="tab-pane fade">
					<form role="form">
					<div class="form-group text-left">
						<label for="text" >BankId:</label>
						<input type="text" class="form-control" id="text" placeholder="BankId">
					</div>
					<div class="form-group text-left">
						<label for="pwd" >Password:</label>
						<input type="password" class="form-control" id="pwd" placeholder="Enter password">
					</div>
					
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				
				</div> --> 
				
				
			
			</div>
			
	
		
		
		</div>
      </div>
      
    </div>
    </div>
    </div>
    </div></center>
    </div>
    </body>
</html>
