// Define a new module for our app
var app = angular.module("indexPage", []);


app.filter('searchUserBloodGroup', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, user_blood_group){

                
		if(!user_blood_group){
			return arr;
		}

		var result = [];

		user_blood_group = user_blood_group.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(user){

			if(user.blood_group.toLowerCase().indexOf(user_blood_group) !== -1){
				result.push(user);
			}

		});

		return result;
	};

});

app.filter('searchUserCity', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, user_city){

		if(!user_city){
			return arr;
		}

		var result = [];

		user_city_city = user_city.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(user){

			if(user.city.toLowerCase().indexOf(user_city) !== -1){
				result.push(user);
			}

		});

		return result;
	};

});

app.filter('searchUserLocation', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, user_location){

		if(!user_location){
			return arr;
		}

		var result = [];

		user_location = user_location.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(user){

			if(user.location.toLowerCase().indexOf(user_location) !== -1){
				result.push(user);
			}

		});

		return result;
	};

});

// Create the instant search filter

app.filter('searchBankName', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, bank_name){

		if(!bank_name){
			return arr;
		}

		var result = [];

		bank_name = bank_name.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(bank){

			if(bank.name.toLowerCase().indexOf(bank_name) !== -1){
				result.push(bank);
			}

		});

		return result;
	};

});

app.filter('searchBankCity', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, bank_city){

		if(!bank_city){
			return arr;
		}

		var result = [];

		bank_city = bank_city.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(bank){

			if(bank.city.toLowerCase().indexOf(bank_city) !== -1){
				result.push(bank);
			}

		});

		return result;
	};

});

app.filter('searchBankLocation', function(){

	// All filters must return a function. The first parameter
	// is the data that is to be filtered, and the second is an
	// argument that may be passed with a colon (searchFor:searchString)

	return function(arr, bank_location){

		if(!bank_location){
			return arr;
		}

		var result = [];

		bank_location = bank_location.toLowerCase();

		// Using the forEach helper method to loop through the array
		angular.forEach(arr, function(bank){
z
			if(bank.location.toLowerCase().indexOf(bank_location) !== -1){
				result.push(bank);
			}

		});

		return result;
	};

});


// The controller

function UserSearchController($scope, $http){

	// The data model. These users would normally be requested via AJAX,
	// but are hardcoded here for simplicity. See the next example for
	// tips on using AJAX.

	$http.get("admin/services/SearchUser")
            .then(function(response) {
                    $scope.users = response.data;
                });

}


// The controller
/*


function BankSearchController($scope, $http){

	// The data model. These banks would normally be requested via AJAX,
	// but are hardcoded here for simplicity. See the next example for
	// tips on using AJAX.

        $http.get("admin/services/SearchBank")
            .then(function(response) {
                    $scope.banks = response.data;
                });

}

*/

function GetCommentController($scope, $http){

	// The data model. These users would normally be requested via AJAX,
	// but are hardcoded here for simplicity. See the next example for
	// tips on using AJAX.

	$http.get("admin/services/GetComment")
            .then(function(response) {
                $scope.comments = response.data;
                
                $scope.current = $scope.comments.length - 1;
                
                $scope.next = function(){
                    $scope.current = $scope.current + 5;
                };
                
                $scope.previous = function(){
                    $scope.current = $scope.current - 5 ;
                };
                $scope.len = $scope.comments.length;
        });
                
}
