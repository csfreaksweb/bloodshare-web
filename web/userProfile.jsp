<%-- 
    Document   : editUser
    Created on : May 9, 2016, 7:25:13 PM
    Author     : shivam_sg
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
        --><script src="js/angular.min.js"></script>
        <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-toggle.min.js"></script>
        <script src = "js/loginform.js"></script>
        
        <style>
          body {
              font: 400 15px Lato, sans-serif;
              line-height: 1.8;
              color: #818181;
          }
          
          #map {
              height: 100%;
          }
            
          .controls {
              margin-top: 10px;
              border: 1px solid transparent;
              border-radius: 2px 0 0 2px;
              box-sizing: border-box;
              -moz-box-sizing: border-box;
              height: 32px;
              outline: none;
              box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            }

            #pac-input {
              background-color: #fff;
              font-family: Roboto;
              font-size: 15px;
              font-weight: 300;
              margin-right: 12px;
              padding: 0 11px 0 13px;
              text-overflow: ellipsis;
              width: 300px;
            }

            #pac-input:focus {
              border-color: #4d90fe;
            }

            .pac-container {
              font-family: Roboto;
            }

            #type-selector {
              color: #fff;
              background-color: #4d90fe;
              padding: 5px 11px 0px 11px;
            }

            #type-selector label {
              font-family: Roboto;
              font-size: 13px;
              font-weight: 300;
            }
            #target {
              width: 345px;
            }
            /*.map-icon-label .map-icon {
                    font-size: 12px;
                    color: #000;
                    line-height: 24px;
                    text-align: center;
                    white-space: nowrap;
            }*/
            .infobox-wrapper {
                 border-radius: 15px;
            }
            #infobox {
                border:2px solid black;
                margin-top: 8px;
                background:#333;
                color:#fff;
                font-family: 'Roboto Condensed', sans-serif;
                font-size:15px;
                padding: .5em 1em;
                -webkit-border-radius: 4px;
                -moz-border-radius: 4px;
                text-shadow:0 -1px #000000;
                -webkit-box-shadow: 10px 10px 5px #888888;
                box-shadow: 10px 10px 5px #888888;
            }
            .mylabel {
                color:red;
            }
          
            h2 {
              font-size: 24px;
              text-transform: uppercase;
              color: #303030;
              font-weight: 600;
              margin-bottom: 30px;
          }
          h4 {
              font-size: 19px;
              line-height: 1.375em;
              color: #303030;
              font-weight: 400;
              margin-bottom: 30px;
          }  
          .jumbotron {
              background-color: #ff4d4d;
              color: #fff;
              padding: 100px 25px;
              font-family: Montserrat, sans-serif;
              max-height: 300px;
          }
          .container-fluid {
              padding: 60px 50px;
          }
          .bg-grey {
              background-color: #f6f6f6;
          }
          .logo-small {
              color: #c30000;
              font-size: 50px;
          }
          .logo {
              color: #c30000;
              font-size: 200px;
          }
          .thumbnail {
              padding: 0 0 15px 0;
              border: none;
              border-radius: 0;
          }
          .thumbnail img {
              width: 100%;
              height: 100%;
              margin-bottom: 10px;
          }
          .carousel-control.right, .carousel-control.left {
              background-image: none;
              color: #c30000;
          }
          .carousel-indicators li {
              border-color: #c30000;
          }
          .carousel-indicators li.active {
              background-color: #c30000;
          }
          .item h4 {
              font-size: 19px;
              line-height: 1.375em;
              font-weight: 400;
              font-style: italic;
              margin: 70px 0;
          }
          .item span {
              font-style: normal;
          }
          .panel {
              border: 1px solid #c30000; 
              border-radius:0 !important;
              transition: box-shadow 0.5s;
          }
          .panel:hover {
              box-shadow: 5px 0px 40px rgba(0,0,0, .2);
          }
          .panel-footer .btn:hover {
              border: 1px solid #c30000;
              background-color: #fff !important;
              color: #c30000;
          }
          .panel-heading {
              color: #fff !important;
              background-color: #c30000 !important;
              padding: 25px;
              border-bottom: 1px solid transparent;
              border-top-left-radius: 0px;
              border-top-right-radius: 0px;
              border-bottom-left-radius: 0px;
              border-bottom-right-radius: 0px;
          }
          .panel-footer {
              background-color: white !important;
          }
          .panel-footer h3 {
              font-size: 32px;
          }
          .panel-footer h4 {
              color: #aaa;
              font-size: 14px;
          }
          .panel-footer .btn {
              margin: 15px 0;
              background-color: #c30000;
              color: #fff;
          }
          .navbar {
              margin-bottom: 0;
              background-color: #c30000;
              z-index: 9999;
              border: 0;
              font-size: 12px !important;
              line-height: 1.42857143 !important;
              letter-spacing: 2px;
              border-radius: 0;
              font-family: Montserrat, sans-serif;
          }
          .navbar li a, .navbar .navbar-brand {
              color: #fff !important;
          }
          .navbar-nav li a:hover, .navbar-nav li.active a {
              color: #c30000 !important;
              background-color: #fff !important;
          }
          .navbar-default .navbar-toggle {
              border-color: transparent;
              color: #fff !important;
          }
          footer .glyphicon {
              font-size: 20px;
              margin-bottom: 20px;
              color: #c30000;
          }
          .slideanim {visibility:hidden;}
          .slide {
              animation-name: slide;
              -webkit-animation-name: slide;	
              animation-duration: 1s;	
              -webkit-animation-duration: 1s;
              visibility: visible;			
          }
          @keyframes slide {
            0% {
              opacity: 0;
              -webkit-transform: translateY(70%);
            } 
            100% {
              opacity: 1;
              -webkit-transform: translateY(0%);
            }	
          }
          @-webkit-keyframes slide {
            0% {
              opacity: 0;
              -webkit-transform: translateY(70%);
            } 
            100% {
              opacity: 1;
              -webkit-transform: translateY(0%);
            }
          }
          @media screen and (max-width: 768px) {
            .col-sm-4 {
              text-align: center;
              margin: 25px 0;
            }
            .btn-lg {
                width: 100%;
                margin-bottom: 35px;
            }
          }
          @media screen and (max-width: 480px) {
            .logo {
                font-size: 150px;
            }
          }
          .inner-addon { 
            position: relative; 
        }

        /* style icon */
        .inner-addon .glyphicon {
          position: absolute;
          padding: 10px;
          pointer-events: none;
        }

        /* align icon */
        .left-addon .glyphicon  { left:  0px;}
        .right-addon .glyphicon { right: 0px;}

        /* add padding  */
        .left-addon input  { padding-left:  30px; }
        .right-addon input { padding-right: 30px; }
        
        .modal {
            text-align: center;
            padding: 0!important;
          }

          .modal:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -4px;
          }

          .modal-dialog {
            display: inline-block;
            text-align: left;
            vertical-align: middle;
          }
        
          .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px; }
          .toggle.ios .toggle-handle { border-radius: 20px; }
        
          footer {
            //position: fixed;
            bottom: 0px;
            width: 100%;
        }
        
        
          .login_signup {
            margin: 40px 25px;
            width: 300px;
            display: block;
            border: none;
            padding: 10px 0;
            border-bottom: solid 1px #C30000;
            -webkit-transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 96%, #C30000 4%);
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 96%, #C30000 4%);
            background-position: -300px 0;
            background-size: 300px 100%;
            background-repeat: no-repeat;
            color: #0e6252;
           }

           .login_signup:focus,
             .login_signup:valid {
             box-shadow: none;
             outline: none;
             background-position: 0 0;
           }

           .login_signup::-webkit-input-placeholder {
             font-family: 'roboto', sans-serif;
             -webkit-transition: all 0.3s ease-in-out;
             transition: all 0.3s ease-in-out;
           }

           .login_signup:focus::-webkit-input-placeholder,
             .login_signup:valid::-webkit-input-placeholder {
             color: #C30000;
             font-size: 11px;
             -webkit-transform: translateY(-20px);
             transform: translateY(-20px);
             visibility: visible !important;
           }
           
           .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
           {
                   color: #C30000;
           }
           ul li{
           list-style-type: none;
           display: inline;
           }
           ul li a{
           color: #555;
           }
           ul li a:hover{
           color: #555;
           }

        </style>
        <script type="text/javascript">
            function editProfile() {
                //console.log("here");
                document.getElementById("user_city").disabled = false;
                document.getElementById("user_location").disabled = false;
                document.getElementById("user_contact").disabled = false;
                document.getElementById("user_alternate_contact").disabled = false;
                document.getElementById("user_email").disabled = false;
                document.getElementById("user_signup_picture").style.display = 'block';
                document.getElementById("saveChanges").style.visibility = 'visible';
                document.getElementById("editProfile").style.visibility = 'hidden';
            }
            
            function saveChanges() {
                //console.log("here");
                if(document.getElementById("user_city").value === "") {
                    document.getElementById("user_city").focus();
                    return;
                }
                if(document.getElementById("user_location").value === "") {
                    document.getElementById("user_location").focus();
                    return;
                }
                if(document.getElementById("user_contact").value === "") {
                    document.getElementById("user_contact").focus();
                    return;
                }
                if(document.getElementById("user_alternate_contact").value === "") {
                    document.getElementById("user_alternate_contact").focus();
                    return;
                }
                if(document.getElementById("user_email").value === "") {
                    document.getElementById("user_email").focus();
                    return;
                }
                
                updateProfile();
                
                
            }
            
                    canvasCtx = null;
        imageFile = null;
        url = null;
        flag=0;

        window.onload = function () {
                canvasCtx = document.getElementById("signup_panel").getContext("2d");
                document.getElementById("user_signup_picture").onchange = function(event) {

                        flag=1;		
                        this.imageFile = event.target.files[0];

                        var reader = new FileReader();
                        reader.onload =  function(event) {
                                var img = new Image();
                                img.onload = function() {
                                        drawImage(img);
                                };
                                img.src = event.target.result;
                        };
                        reader.readAsDataURL(this.imageFile);
                };

                drawImage = function(img) {
                        this.canvasCtx.canvas.width = 200;
                        this.canvasCtx.canvas.height = 250;
                        this.canvasCtx.drawImage(img,0,0,200,250);
                        url = canvasCtx.canvas.toDataURL("image/png",0.7);
                        //console.log(url);
                        flag=1;
                        setURL(url,flag);
                        //console.log(document.getElementById("hidden").value);
                };
        };

        function setURL(url,flag)
        {
            document.getElementById("signup_hidden").value= url;	
        }


            
            function updateProfile() {
                var user_email = document.getElementById("user_email").value;
                var user_city = document.getElementById("user_city").value;
                var user_location = document.getElementById("user_location").value;
                var user_contact = document.getElementById("user_contact").value;
                var user_alternate_contact = document.getElementById("user_alternate_contact").value;
                var user_pic = document.getElementById("signup_hidden").value;
                //console.log(user_pic);
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/UpdateProfile";

                var data = new Object();

                data["user_email"] = user_email;
                data["user_pic"] = user_pic;
                //console.log(user_pic);
                data["user_city"] = user_city;
                data["user_location"] = user_location;
                data["user_contact"] = user_contact;
                data["user_alternate_contact"] = user_alternate_contact;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "0") {
                                document.getElementById("user_city").disabled = true;
                                document.getElementById("user_location").disabled = true;
                                document.getElementById("user_contact").disabled = true;
                                document.getElementById("user_alternate_contact").disabled = true;
                                document.getElementById("user_email").disabled = true;
                                document.getElementById("user_signup_picture").style.display = 'none';
                                document.getElementById("saveChanges").style.visibility = 'hidden';
                                document.getElementById("editProfile").style.visibility = 'visible';
                                location.reload(true);
                            }
                            if(json.return_code === "1048") {
                                $("#myLoginModal").modal('show');
                            }
                            //console.log(json.return_code);
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
        
            $(function() {
              $('#toggle-event').change(function() {
                    changeStatus($(this).prop('checked'));
              });
            });
            
            function changeStatus(status) {
                var user_available = status;
                
                if(status) user_available = "1";
                else user_available = "0";
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/ChangeStatus";

                var data = new Object();

                data["user_available"] = user_available;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "1048") {
                                $("#myLoginModal").modal('show');
                            }
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
        
            function changePassword() {
                
                if(document.getElementById("user_password").value === "") {
                    document.getElementById("user_password").focus();
                    return;
                }
                if(document.getElementById("user_confirm_password").value === "") {
                    document.getElementById("user_confirm_password").focus();
                    return;
                }
                
                var user_password = document.getElementById("user_password").value;
                var user_confirm_password = document.getElementById("user_confirm_password").value;
                
                if(user_confirm_password !== user_password){
                    document.getElementById("user_confirm_password").focus();
                    return ;
                }
                
                xmlhttp=new XMLHttpRequest();
                var url = "admin/services/ChangeUserPassword";

                var data = new Object();

                data["user_password"] = user_password;
                
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            var json = JSON.parse(xmlhttp.responseText);
                            if(json.return_code === "0") {
                                //$("#myModal").hide();
                                $('#myModal').modal('hide');
                                $("#mysecondModal").modal();
                            }
                            else {
                                $('#myModal').modal('hide');
                                $("#myLoginModal").modal('show');
                            }
                        }
                };

                xmlhttp.open("POST", url, true);
                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                xmlhttp.send(JSON.stringify(data));	
            }
        
        $(document).ready( function () {
            //Search the parent class, id, or tag and then try to find the <a id="addMore"></a>  as a child
            $('#changePassword').on('click', function () {
                changePassword();
            });
        });
        </script>
        <title>User Profile</title>
    </head>
    <body>
        
        <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
            <a class="navbar-brand" href="#myPage"><span class="glyphicon glyphicon-tint"></span>BloodShare</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
            <li><a href='index.jsp#banks' id="banks"><span class="glyphicon glyphicon-search"></span>BLOOD BANKS</a></li>
            <li><a href="index.jsp#donors" id="donors"><span class="glyphicon glyphicon-search"></span>DONORS</a></li>
            <li><a href="index.jsp#request" id="request">REQUEST BLOOD</a></li>
            <li><a href="index.jsp#forum" id="forum"><span class="glyphicon glyphicon-pencil"></span>FORUM</a></li>
            <li><a href="index.jsp#contact" id="contact"><span class="glyphicon glyphicon-earphone"></span>Contact</a></li>
            <li><%
                    if(session.getAttribute("session_name") != null) {
                        if(((String)session.getAttribute("session_type")).equals("user")) out.println("<a href='#'><span class='glyphicon glyphicon-user'></span>"+session.getAttribute("session_name")+"</a>");
                        else out.println("<a href='#'><span class='glyphicon glyphicon-briefcase'></span>Bank_id: "+session.getAttribute("session_name")+"</a>");
                    }
                %>
            </li>
            <li><%
                    if(session.getAttribute("session_name") == null) out.println("<a id='loginlink' href='#'><span class='glyphicon glyphicon-user'></span> Sign Up/Login</a>");
                    else out.println("<a href='login/banks/logoutBank' id='logout'><span class='glyphicon glyphicon-user'></span> Logout</a>");
                %>
            </li>
          </ul>
        </div>
      </div>
    </nav>
        
        <%
                    Map user_data = (HashMap)request.getAttribute("UserData");
        %>
        
        <div id="request" class="container-fluid">
        <h3  class="text-center"><% out.println(user_data.get("user_name"));%></h3>
        <br>
        <div class="row">
              <div class="col-sm-5">
                  <div class="row">
                  <div class="col-sm-2 form-group">
                  </div>
                  <div class="col-sm-10 form-group">
                      <img src="<% out.println(user_data.get("user_pic"));%>" height="200px" width="200px"  class="img-thumbnail"><input  class="login_signup" style="display: none;" type="file" accept="image/*" id="user_signup_picture" placeholder="Upload Pic"><br><font style="font-size:30px;"><% out.println(user_data.get("user_first_name")+" "+user_data.get("user_last_name"));%>&nbsp; &nbsp; &nbsp; <span class="glyphicon glyphicon-star-empty" style="color:#c30000;"></span><% out.println(user_data.get("user_thanks_vote"));%> </font><br>
                    <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name")))){
                                    if(((String)user_data.get("user_available")).equals("0")) 
                                        out.println("<input id='toggle-event' type='checkbox' data-toggle='toggle' data-on='Available' data-off='Unavailable' data-size='small' data-style='ios' data-onstyle='success' data-offstyle='danger'>&nbsp; <font style='font-size: small;'>(availability for Blood donation)</font><br><font style='font-size: x-small;'>(Toggle here to change)</font><br>");
                                    else
                                        out.println("<input id='toggle-event' type='checkbox' data-toggle='toggle' checked data-on='Available' data-off='Unavailable' data-size='small' data-style='ios' data-onstyle='success' data-offstyle='danger'>&nbsp; <font style='font-size: small;'>(availability for Blood donation)</font><br><font style='font-size: x-small;'>(Toggle here to change)</font><br>");
                                }
                                else {
                                        if(((String)user_data.get("user_available")).equals("0")) 
                                            out.println("<div style='height: 30px; width: 110px; background-color:#c9302c; border-color:#ac2925; color:white; border-radius:15px;' class='text-center'>Unavailable</div>");
                                        else    
                                            out.println("<div style='height: 30px; width: 110px; background-color:#5cb85c;; border-color:#4cae4c; color:white; border-radius:15px;' class='text-center'>Available</div>");
                                } 
                            }
                            else {
                                        if(((String)user_data.get("user_available")).equals("0")) 
                                            out.println("<div style='height: 30px; width: 110px; background-color:#c9302c; border-color:#ac2925; color:white; border-radius:15px;' class='text-center'>Unavailable</div>");
                                        else    
                                            out.println("<div style='height: 30px; width: 110px; background-color:#5cb85c;; border-color:#4cae4c; color:white; border-radius:15px;' class='text-center'>Available</div>");
                                } 
                    %>
                    Gender:<font style="color: black;"> <% out.println(user_data.get("user_gender"));%></font> <br>
                    Blood Group:<font style="color: black;"> <% out.println(user_data.get("user_blood_group"));%></font>
                  </div>
                  </div>
              </div>
              <div class="col-sm-7">
                  <div class="row">
                  <div class="col-sm-6 form-group">
                  </div>
                  <div class="col-sm-6 form-group">
                      <%
                        if(session.getAttribute("session_name") != null) {
                            if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name"))))
                                out.println("<button type='button' id='editProfile' class='btn btn-grey pull-right' onclick='editProfile()'><span class='glyphicon glyphicon-edit'></span> Edit Profile</button>");
                        }
                      %>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                      <label for="text">City:</label>
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name"))))
                                    out.println("<input class='form-control' id='user_city' name='user_city' placeholder='City' type='text' disabled value='"+user_data.get("user_city")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_city")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_city")+"</font>");
                            
                      %>
                  </div>
                  <div class="col-sm-6 form-group">
                      <label for="text">Location:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name"))))
                                    out.println("<input class='form-control' id='user_location' name='user_location' type='text' disabled value='"+user_data.get("user_location")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_location")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_location")+"</font>");
                            
                      %>
                  </div>
                </div>
                 <div class="row">
                  <div class="col-sm-6 form-group">
                      <label for="text">Contact:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name"))))
                                    out.println("<input class='form-control' id='user_contact' name='user_contact' type='text' disabled value='"+user_data.get("user_contact")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_contact")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_contact")+"</font>");
                            
                      %>
                  </div>
                    <div class="col-sm-6 form-group">
                      <label for="text">Alternate Contact:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name"))))
                                    out.println("<input class='form-control' id='user_alternate_contact' name='user_alternate_contact' type='text' disabled value='"+user_data.get("user_alternate_contact")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_alternate_contact")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_alternate_contact")+"</font>");
                            
                      %>
                </div>
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                      <label for="text">Email:</label>
                  
                      <%
                            if(session.getAttribute("session_name") != null) {
                                if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name"))))
                                    out.println("<input class='form-control' id='user_email' name='user_email' type='text' disabled value='"+user_data.get("user_email")+"' required>");
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_email")+"</font>");
                            }
                            else
                                out.println("&nbsp;<font style='color: black; font-size: 20px;'>"+user_data.get("user_email")+"</font>");
                      %>
                  </div>
                  <div class="col-sm-6 form-group">
                      <%
                        if(session.getAttribute("session_name") != null) {
                            if(((String)session.getAttribute("session_name")).equals((String)(user_data.get("user_name"))))
                                out.println("<br><button type='button'  class='btn btn-grey pull-right'  data-toggle='modal' data-target='#myModal'><span class='glyphicon glyphicon-lock'></span> Change Password</button>");
                        }
                      %>
                  </div>
                
                </div>
                <div class="row">
                  <div class="col-sm-6 form-group">
                      <button id="saveChanges" class="btn btn-primary pull-left" style="visibility: hidden;" onclick="saveChanges()" type="button">Save Changes</button>
                  </div>
                </div>	
              </div>
        </div>
    </div>
    <footer class="container-fluid bg-grey text-center">
     
        <p>&copy; Copyright 2016 by Team BloodShare. All Rights Reserved.</p>		
    </footer>
    
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title text-center">Reset Password</h3>
        </div>
        <div class="modal-body">
          <form>
             <label>Password:</label>
             <input type="password" id="user_password" name="user_password" class="form-control">
             <label>Confirm Password:</label>
             <input type="password" id="user_confirm_password" name="user_confirm_password" class="form-control">
             <br>
             <button type='button' id='changePassword' class='form-control btn-danger'><span class='glyphicon glyphicon-lock'>                    
             </span> Change Password</button>
        </form>        
       </div>
      </div>
    </div>
  </div>
</div>

                  
                  
<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myHiddenModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title text-center">Reset Password</h3>
        </div>
        <div class="modal-body">
          <form>
                <canvas id="signup_panel" height="250" width="200"></canvas>
                <input type="hidden" id="signup_hidden" name="hidden" value="none">
                                            
          </form>        
       </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

    <script>
        if(document.getElementById("banks"))
        {
            document.getElementById("banks").onclick = function () 
            {
                location.href = "index.jsp#banks";
            };
        }
        if(document.getElementById("donors"))
        {
            document.getElementById("donors").onclick = function () 
            {
                location.href = "index.jsp#donors";
            };
        }
        if(document.getElementById("request"))
        {
            document.getElementById("request").onclick = function () 
            {
                location.href = "index.jsp#request";
            };
        }
        if(document.getElementById("forum"))
        {
            document.getElementById("forum").onclick = function () 
            {
                location.href = "index.jsp#forum";
            };
        }
        if(document.getElementById("contact"))
        {
            document.getElementById("contact").onclick = function () 
            {
                location.href = "index.jsp#contact";
            };
        }
        if(document.getElementById("logout")){
            document.getElementById("logout").onclick = function () {
                location.href = "login/banks/logoutBank";
            };
        }

        if(document.getElementById("loginlink")){
            document.getElementById("loginlink").onclick = function () {
                $("#LoginForm").modal("show");
            };
        }
    </script>
    <div class="modal fade" id="mysecondModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color: #c30000;">Your password has been changed successfully.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
    
    <div class="modal fade" id="myLoginModal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title text-center" style="color: #c30000;">You must Login first to complete this action.</h4>
               <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
            </div>
            <!--<div class="modal-body">
              <p>Some text in the modal.</p>
            </div>-->
          </div>

        </div>
      </div>
    
    
    
    
    </body>
</html>
